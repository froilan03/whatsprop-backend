require 'test_helper'

class RoomFurnishingsControllerTest < ActionController::TestCase
  setup do
    @room_furnishing = room_furnishings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:room_furnishings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create room_furnishing" do
    assert_difference('RoomFurnishing.count') do
      post :create, room_furnishing: { description: @room_furnishing.description, name: @room_furnishing.name }
    end

    assert_redirected_to room_furnishing_path(assigns(:room_furnishing))
  end

  test "should show room_furnishing" do
    get :show, id: @room_furnishing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @room_furnishing
    assert_response :success
  end

  test "should update room_furnishing" do
    patch :update, id: @room_furnishing, room_furnishing: { description: @room_furnishing.description, name: @room_furnishing.name }
    assert_redirected_to room_furnishing_path(assigns(:room_furnishing))
  end

  test "should destroy room_furnishing" do
    assert_difference('RoomFurnishing.count', -1) do
      delete :destroy, id: @room_furnishing
    end

    assert_redirected_to room_furnishings_path
  end
end

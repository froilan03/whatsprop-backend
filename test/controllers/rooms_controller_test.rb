require 'test_helper'

class RoomsControllerTest < ActionController::TestCase
  setup do
    @room = rooms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rooms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create room" do
    assert_difference('Room.count') do
      post :create, room: { cooking: @room.cooking, location_id: @room.location_id, price: @room.price, property_type_id: @room.property_type_id, room_bath_id: @room.room_bath_id, room_category_id: @room.room_category_id, room_furnishing_id: @room.room_furnishing_id, room_lease_id: @room.room_lease_id, share_living_room: @room.share_living_room }
    end

    assert_redirected_to room_path(assigns(:room))
  end

  test "should show room" do
    get :show, id: @room
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @room
    assert_response :success
  end

  test "should update room" do
    patch :update, id: @room, room: { cooking: @room.cooking, location_id: @room.location_id, price: @room.price, property_type_id: @room.property_type_id, room_bath_id: @room.room_bath_id, room_category_id: @room.room_category_id, room_furnishing_id: @room.room_furnishing_id, room_lease_id: @room.room_lease_id, share_living_room: @room.share_living_room }
    assert_redirected_to room_path(assigns(:room))
  end

  test "should destroy room" do
    assert_difference('Room.count', -1) do
      delete :destroy, id: @room
    end

    assert_redirected_to rooms_path
  end
end

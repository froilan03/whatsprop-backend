require 'test_helper'

class PropertyPrizeRangesControllerTest < ActionController::TestCase
  setup do
    @property_prize_range = property_prize_ranges(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:property_prize_ranges)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create property_prize_range" do
    assert_difference('PropertyPrizeRange.count') do
      post :create, property_prize_range: { description: @property_prize_range.description, name: @property_prize_range.name }
    end

    assert_redirected_to property_prize_range_path(assigns(:property_prize_range))
  end

  test "should show property_prize_range" do
    get :show, id: @property_prize_range
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @property_prize_range
    assert_response :success
  end

  test "should update property_prize_range" do
    patch :update, id: @property_prize_range, property_prize_range: { description: @property_prize_range.description, name: @property_prize_range.name }
    assert_redirected_to property_prize_range_path(assigns(:property_prize_range))
  end

  test "should destroy property_prize_range" do
    assert_difference('PropertyPrizeRange.count', -1) do
      delete :destroy, id: @property_prize_range
    end

    assert_redirected_to property_prize_ranges_path
  end
end

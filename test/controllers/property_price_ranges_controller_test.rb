require 'test_helper'

class PropertyPriceRangesControllerTest < ActionController::TestCase
  setup do
    @property_price_range = property_price_ranges(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:property_price_ranges)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create property_price_range" do
    assert_difference('PropertyPriceRange.count') do
      post :create, property_price_range: { description: @property_price_range.description, name: @property_price_range.name }
    end

    assert_redirected_to property_price_range_path(assigns(:property_price_range))
  end

  test "should show property_price_range" do
    get :show, id: @property_price_range
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @property_price_range
    assert_response :success
  end

  test "should update property_price_range" do
    patch :update, id: @property_price_range, property_price_range: { description: @property_price_range.description, name: @property_price_range.name }
    assert_redirected_to property_price_range_path(assigns(:property_price_range))
  end

  test "should destroy property_price_range" do
    assert_difference('PropertyPriceRange.count', -1) do
      delete :destroy, id: @property_price_range
    end

    assert_redirected_to property_price_ranges_path
  end
end

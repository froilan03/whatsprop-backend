require 'test_helper'

class PropertyYearConstructedsControllerTest < ActionController::TestCase
  setup do
    @property_year_constructed = property_year_constructeds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:property_year_constructeds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create property_year_constructed" do
    assert_difference('PropertyYearConstructed.count') do
      post :create, property_year_constructed: { description: @property_year_constructed.description, name: @property_year_constructed.name }
    end

    assert_redirected_to property_year_constructed_path(assigns(:property_year_constructed))
  end

  test "should show property_year_constructed" do
    get :show, id: @property_year_constructed
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @property_year_constructed
    assert_response :success
  end

  test "should update property_year_constructed" do
    patch :update, id: @property_year_constructed, property_year_constructed: { description: @property_year_constructed.description, name: @property_year_constructed.name }
    assert_redirected_to property_year_constructed_path(assigns(:property_year_constructed))
  end

  test "should destroy property_year_constructed" do
    assert_difference('PropertyYearConstructed.count', -1) do
      delete :destroy, id: @property_year_constructed
    end

    assert_redirected_to property_year_constructeds_path
  end
end

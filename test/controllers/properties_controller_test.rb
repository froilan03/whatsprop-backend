require 'test_helper'

class PropertiesControllerTest < ActionController::TestCase
  setup do
    @property = properties(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:properties)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create property" do
    assert_difference('Property.count') do
      post :create, property: { amenities: @property.amenities, developer: @property.developer, features: @property.features, name: @property.name, number_of_bath: @property.number_of_bath, number_of_bed: @property.number_of_bed, postal_code: @property.postal_code, price: @property.price, property_category_id: @property.property_category_id, property_location_id: @property.property_location_id, property_tenure_id: @property.property_tenure_id, property_type_id: @property.property_type_id, psf: @property.psf, size: @property.size, year_constructed: @property.year_constructed }
    end

    assert_redirected_to property_path(assigns(:property))
  end

  test "should show property" do
    get :show, id: @property
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @property
    assert_response :success
  end

  test "should update property" do
    patch :update, id: @property, property: { amenities: @property.amenities, developer: @property.developer, features: @property.features, name: @property.name, number_of_bath: @property.number_of_bath, number_of_bed: @property.number_of_bed, postal_code: @property.postal_code, price: @property.price, property_category_id: @property.property_category_id, property_location_id: @property.property_location_id, property_tenure_id: @property.property_tenure_id, property_type_id: @property.property_type_id, psf: @property.psf, size: @property.size, year_constructed: @property.year_constructed }
    assert_redirected_to property_path(assigns(:property))
  end

  test "should destroy property" do
    assert_difference('Property.count', -1) do
      delete :destroy, id: @property
    end

    assert_redirected_to properties_path
  end
end

require 'test_helper'

class RoomShareGendersControllerTest < ActionController::TestCase
  setup do
    @room_share_gender = room_share_genders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:room_share_genders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create room_share_gender" do
    assert_difference('RoomShareGender.count') do
      post :create, room_share_gender: { description: @room_share_gender.description, name: @room_share_gender.name }
    end

    assert_redirected_to room_share_gender_path(assigns(:room_share_gender))
  end

  test "should show room_share_gender" do
    get :show, id: @room_share_gender
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @room_share_gender
    assert_response :success
  end

  test "should update room_share_gender" do
    patch :update, id: @room_share_gender, room_share_gender: { description: @room_share_gender.description, name: @room_share_gender.name }
    assert_redirected_to room_share_gender_path(assigns(:room_share_gender))
  end

  test "should destroy room_share_gender" do
    assert_difference('RoomShareGender.count', -1) do
      delete :destroy, id: @room_share_gender
    end

    assert_redirected_to room_share_genders_path
  end
end

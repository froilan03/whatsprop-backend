require 'test_helper'

class RoomLeasesControllerTest < ActionController::TestCase
  setup do
    @room_lease = room_leases(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:room_leases)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create room_lease" do
    assert_difference('RoomLease.count') do
      post :create, room_lease: { description: @room_lease.description, name: @room_lease.name }
    end

    assert_redirected_to room_lease_path(assigns(:room_lease))
  end

  test "should show room_lease" do
    get :show, id: @room_lease
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @room_lease
    assert_response :success
  end

  test "should update room_lease" do
    patch :update, id: @room_lease, room_lease: { description: @room_lease.description, name: @room_lease.name }
    assert_redirected_to room_lease_path(assigns(:room_lease))
  end

  test "should destroy room_lease" do
    assert_difference('RoomLease.count', -1) do
      delete :destroy, id: @room_lease
    end

    assert_redirected_to room_leases_path
  end
end

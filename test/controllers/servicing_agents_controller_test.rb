require 'test_helper'

class ServicingAgentsControllerTest < ActionController::TestCase
  setup do
    @servicing_agent = servicing_agents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:servicing_agents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create servicing_agent" do
    assert_difference('ServicingAgent.count') do
      post :create, servicing_agent: { description: @servicing_agent.description, price: @servicing_agent.price, title: @servicing_agent.title, user_id: @servicing_agent.user_id }
    end

    assert_redirected_to servicing_agent_path(assigns(:servicing_agent))
  end

  test "should show servicing_agent" do
    get :show, id: @servicing_agent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @servicing_agent
    assert_response :success
  end

  test "should update servicing_agent" do
    patch :update, id: @servicing_agent, servicing_agent: { description: @servicing_agent.description, price: @servicing_agent.price, title: @servicing_agent.title, user_id: @servicing_agent.user_id }
    assert_redirected_to servicing_agent_path(assigns(:servicing_agent))
  end

  test "should destroy servicing_agent" do
    assert_difference('ServicingAgent.count', -1) do
      delete :destroy, id: @servicing_agent
    end

    assert_redirected_to servicing_agents_path
  end
end

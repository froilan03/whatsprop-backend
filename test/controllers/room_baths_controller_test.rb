require 'test_helper'

class RoomBathsControllerTest < ActionController::TestCase
  setup do
    @room_bath = room_baths(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:room_baths)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create room_bath" do
    assert_difference('RoomBath.count') do
      post :create, room_bath: { description: @room_bath.description, name: @room_bath.name }
    end

    assert_redirected_to room_bath_path(assigns(:room_bath))
  end

  test "should show room_bath" do
    get :show, id: @room_bath
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @room_bath
    assert_response :success
  end

  test "should update room_bath" do
    patch :update, id: @room_bath, room_bath: { description: @room_bath.description, name: @room_bath.name }
    assert_redirected_to room_bath_path(assigns(:room_bath))
  end

  test "should destroy room_bath" do
    assert_difference('RoomBath.count', -1) do
      delete :destroy, id: @room_bath
    end

    assert_redirected_to room_baths_path
  end
end

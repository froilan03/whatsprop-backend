require 'test_helper'

class PropertyPsfsControllerTest < ActionController::TestCase
  setup do
    @property_psf = property_psfs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:property_psfs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create property_psf" do
    assert_difference('PropertyPsf.count') do
      post :create, property_psf: { description: @property_psf.description, name: @property_psf.name }
    end

    assert_redirected_to property_psf_path(assigns(:property_psf))
  end

  test "should show property_psf" do
    get :show, id: @property_psf
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @property_psf
    assert_response :success
  end

  test "should update property_psf" do
    patch :update, id: @property_psf, property_psf: { description: @property_psf.description, name: @property_psf.name }
    assert_redirected_to property_psf_path(assigns(:property_psf))
  end

  test "should destroy property_psf" do
    assert_difference('PropertyPsf.count', -1) do
      delete :destroy, id: @property_psf
    end

    assert_redirected_to property_psfs_path
  end
end

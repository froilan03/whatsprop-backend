require 'test_helper'

class PropertySizesControllerTest < ActionController::TestCase
  setup do
    @property_size = property_sizes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:property_sizes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create property_size" do
    assert_difference('PropertySize.count') do
      post :create, property_size: { description: @property_size.description, name: @property_size.name }
    end

    assert_redirected_to property_size_path(assigns(:property_size))
  end

  test "should show property_size" do
    get :show, id: @property_size
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @property_size
    assert_response :success
  end

  test "should update property_size" do
    patch :update, id: @property_size, property_size: { description: @property_size.description, name: @property_size.name }
    assert_redirected_to property_size_path(assigns(:property_size))
  end

  test "should destroy property_size" do
    assert_difference('PropertySize.count', -1) do
      delete :destroy, id: @property_size
    end

    assert_redirected_to property_sizes_path
  end
end

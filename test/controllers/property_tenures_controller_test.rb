require 'test_helper'

class PropertyTenuresControllerTest < ActionController::TestCase
  setup do
    @property_tenure = property_tenures(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:property_tenures)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create property_tenure" do
    assert_difference('PropertyTenure.count') do
      post :create, property_tenure: { description: @property_tenure.description, name: @property_tenure.name }
    end

    assert_redirected_to property_tenure_path(assigns(:property_tenure))
  end

  test "should show property_tenure" do
    get :show, id: @property_tenure
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @property_tenure
    assert_response :success
  end

  test "should update property_tenure" do
    patch :update, id: @property_tenure, property_tenure: { description: @property_tenure.description, name: @property_tenure.name }
    assert_redirected_to property_tenure_path(assigns(:property_tenure))
  end

  test "should destroy property_tenure" do
    assert_difference('PropertyTenure.count', -1) do
      delete :destroy, id: @property_tenure
    end

    assert_redirected_to property_tenures_path
  end
end

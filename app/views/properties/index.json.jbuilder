json.array!(@properties) do |property|
  json.extract! property, :id, :name, :postal_code, :number_of_bed, :number_of_bath, :year_constructed, :price, :size, :developer, :features, :amenities, :psf, :property_type_id, :property_tenure_id, :property_category_id, :property_location_id
  json.url property_url(property, format: :json)
end

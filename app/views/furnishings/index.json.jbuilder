json.array!(@room_furnishings) do |room_furnishing|
  json.extract! room_furnishing, :id, :name, :description
  json.url room_furnishing_url(room_furnishing, format: :json)
end

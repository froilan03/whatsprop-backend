json.array!(@property_year_constructeds) do |property_year_constructed|
  json.extract! property_year_constructed, :id, :name, :description
  json.url property_year_constructed_url(property_year_constructed, format: :json)
end

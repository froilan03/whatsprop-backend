json.array!(@property_tenures) do |property_tenure|
  json.extract! property_tenure, :id, :name, :description
  json.url property_tenure_url(property_tenure, format: :json)
end

json.array!(@room_baths) do |room_bath|
  json.extract! room_bath, :id, :name, :description
  json.url room_bath_url(room_bath, format: :json)
end

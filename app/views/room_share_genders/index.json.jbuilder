json.array!(@room_share_genders) do |room_share_gender|
  json.extract! room_share_gender, :id, :name, :description
  json.url room_share_gender_url(room_share_gender, format: :json)
end

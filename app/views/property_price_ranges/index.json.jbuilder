json.array!(@property_price_ranges) do |property_price_range|
  json.extract! property_price_range, :id, :name, :description
  json.url property_price_range_url(property_price_range, format: :json)
end

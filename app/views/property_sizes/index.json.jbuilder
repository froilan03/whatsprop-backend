json.array!(@property_sizes) do |property_size|
  json.extract! property_size, :id, :name, :description
  json.url property_size_url(property_size, format: :json)
end

json.result true
json.code 200
json.data do
	json.array!(@servicing_agents) do |servicing_agent|
	  json.extract! servicing_agent, :id, :title, :description, :price
	  json.created_at date_format servicing_agent.created_at
	  json.updated_at date_format servicing_agent.updated_at
	
	  json.user do
	  	json.id servicing_agent.user.id
	  	json.username servicing_agent.user.username
	  	json.email servicing_agent.user.email
	  	json.mobile servicing_agent.user.mobile
	  	json.avatar_url add_host_prefix servicing_agent.user.avatar.url(:original, false)
		json.user_role servicing_agent.user.user_role.nil? ? nil : servicing_agent.user.user_role.name
		json.auth_token servicing_agent.user.authentication_token
	  end
	end
end
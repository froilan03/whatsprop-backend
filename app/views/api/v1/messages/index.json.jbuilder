json.result true
json.code 200
json.data do
	json.message do
		json.extract! @message, :id, :sender_id, :recipient_id, :body, :status, :post_id, :created_at
	end
end

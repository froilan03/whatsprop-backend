json.result true
json.code 200
json.data do
	current_user = @user
	json.user do
		#json.extract! current_user, :id, :username, :first_name, :last_name, :email, :avatar_file_name, :mobile, :user_role_id, :created_at, :updated_at
		json.uid current_user.id
		json.first_name current_user.first_name
		json.last_name current_user.last_name
		json.email current_user.email
		json.username current_user.username
		json.mobile current_user.mobile
		json.user_role current_user.user_role.nil? ? nil : current_user.user_role.name
		 
		json.avatar do
			json.thumb add_host_prefix current_user.avatar.url(:thumb, false)
			json.medium add_host_prefix current_user.avatar.url(:medium, false)
			json.original add_host_prefix current_user.avatar.url(:original, false) 
		end 
		
		json.auth_token current_user.authentication_token
		json.created_at current_user.created_at
		json.settings do
			json.array!(Setting.all) do |setting|
		  		json.extract! setting, :variable, :value
			end
		end
		
		unless current_user.agent_info.nil?
				json.agent_info do
					json.extract! current_user.agent_info, :reg_no, :estate_agent_name, :reg_period_start, :reg_period_end, :estate_agent_license_number
				end
			else
				json.agent_info nil	
			end
		
	end
	
end
 

 


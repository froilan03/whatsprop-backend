json.result true
json.code 200
json.data do
	json.array!(@regions) do |region|
		json.id region.city_id
	  	json.name region.city_name
	  	
	  	json.sub_cities do
	  		json.array!(region.cities) do |city|
	  			json.id city.subcity_id
	  			json.name city.subcity_name
	  		end
	  	end
	end
end
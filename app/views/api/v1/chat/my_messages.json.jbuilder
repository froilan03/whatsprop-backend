json.result true
json.code 200
json.data do
	json.array!(@messages) do |message|
	  @post = Post.find_by_id(message.post_id)
	  
	  json.message_id message.id
      json.post_title @post.title  unless @post.nil?
      json.post_id @post.id  unless @post.nil?
	  json.message message.body
	  
	  if params[:message_type] == 'sent'
	  	json.user_id message.recipient_id
	  	@user = User.find_by_id message.recipient_id
	  else
	  	json.user_id message.sender_id
	  	@user = User.find_by_id message.sender_id
	  end
	  
	  json.avatar add_host_prefix @user.avatar.url(:thumb, false)
	  json.date_sent date_format message.created_at
	end
end
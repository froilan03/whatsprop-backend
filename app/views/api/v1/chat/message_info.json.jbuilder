json.result true
json.code 200
json.data do
	
	  @post = Post.find_by_id(@message.post_id)
	  
	  json.message_id @message.id
      json.post_title @post.title unless @post.nil?
      json.post_id @post.id unless @post.nil?
	  json.message @message.body
	  
	  @recipient = User.find_by_id @message.recipient_id
	  json.recipient do
	  	json.id @message.recipient_id
	  	json.first_name @recipient.first_name
	  	json.last_name @recipient.last_name
	  	json.email @recipient.email
	  	json.avatar add_host_prefix @recipient.avatar.url(:thumb, false)
	  end
	  
	  @sender = User.find_by_id @message.sender_id
	  json.sender do
	  	json.id @message.sender_id
	  	json.first_name @sender.first_name
	  	json.last_name @sender.last_name
	  	json.email @sender.email
	  	json.avatar add_host_prefix @sender.avatar.url(:thumb, false)
	  end
	  
	  json.date_sent date_format @message.created_at
end
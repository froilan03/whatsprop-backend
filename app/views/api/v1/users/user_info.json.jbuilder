json.success true
json.code 200
json.data do
	json.user do
		json.uid @user.id
		json.first_name @user.first_name
		json.last_name @user.last_name
		json.email @user.email
		json.mobile @user.mobile
		json.username @user.username
		json.avatar do
			json.thumb add_host_prefix @user.avatar.url(:thumb, false)
			json.medium add_host_prefix @user.avatar.url(:medium, false)
			json.original add_host_prefix @user.avatar.url(:original, false) 
		end 
		
		json.user_role @user.user_role.nil? ? nil : @user.user_role.name
		json.auth_token @user.authentication_token
		json.created_at @user.created_at
		unless @user.agent_info.nil?
				json.agent_info do
					json.extract! @user.agent_info, :reg_no, :estate_agent_name, :reg_period_start, :reg_period_end, :estate_agent_license_number
				end
			else
				json.agent_info nil	
			end
	end	
end
json.result true
json.code 200
json.data do
	json.array!(@user_roles) do |role|
  		json.extract! role, :id, :name, :description
	end
end
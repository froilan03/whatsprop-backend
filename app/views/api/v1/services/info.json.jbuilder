json.result true
json.code 200
json.data do
	json.extract! @service, :id, :description, :price, :user_id
	json.created_at date_format @service.created_at
	json.updated_at date_format @service.updated_at
end
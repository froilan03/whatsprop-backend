json.result true
json.code 200
json.data do
	json.array!(@posts) do |post|
		json.post do
			json.id post.id
			json.title post.title
			json.description post.description
			json.status post.status
			json.expiry_date date_format(post.expiry_date)
			json.post_type (post.property.nil? ? 'room' : 'property')
			
			json.bookmark do
				if params[:user_id].present?
					b = Bookmark.get_bookmarked params[:user_id], post.id
					json.id b.first.nil? ? nil : b.first.id 
					json.value b.first.nil? ? "0" : "1"
				else
					json.id nil
					json.value "0"
				end			
			end
			
			json.user do
				json.extract! post.user, :id, :first_name, :last_name, :email, :username, :mobile
				json.avatar_url post.user.avatar.url(:thumb, false)
			end
			
			@property = post.property
			unless @property.nil?
				json.property do
					json.extract! @property, :id, :name, :price, :psf, :size, :number_of_bed, :number_of_bath, :year_constructed, :developer, :features, :amenities
					
					unless @property.property_category.nil?
						json.category do
							json.id @property.property_category.id
							json.name @property.property_category.name
						end
					else
						json.category nil
					end
					
					unless @property.property_type.nil?
					   json.type do
					   		json.id @property.property_type.id
					   		json.name @property.property_type.name
					   end
					else
						json.type nil
					end
					
					unless @property.property_tenure.nil?
						json.tenure do
							json.id @property.property_tenure.id
							json.name @property.property_tenure.name
						end
					else
						json.tenure nil
					end
					 
					unless @property.lease_term.nil?
						json.lease_term do
							json.id @property.lease_term.id
							json.name @property.lease_term.name
						end
					else
						json.lease_term nil					
					end
					 
					unless @property.furnishing.nil?
						json.furnishing do
							json.id @property.furnishing.id
							json.name @property.furnishing.name
						end
					else
						json.furnishing nil
					end
					
					@location = @property.location
					unless @property.location.nil?
						json.address @location.address
						json.latitude @location.latitude
						json.longitude @location.longitude
					else
						json.address nil
						json.latitude nil
						json.longitude nil
					end
					
					@region = @property.region
					json.region do
						unless @region.nil?
							json.id @region.city_id
							json.name @region.city_name
						else
							json.id nil
							json.name nil
						end
					end
					
					@city = @property.city
					json.city do
						unless @city.nil?
							json.id @city.subcity_id
							json.name @city.subcity_name
						else
							json.id nil
							json.name nil
						end
					end	
				end
				
				if @property.location.present?
					json.badge_count Post.get_location_count @property.location.latitude, @property.location.latitude
				else
					json.badge_count 0
				end
			end	
			
			@room = post.room
			unless @room.nil?
				json.room do
					json.extract! @room, :id, :price, :cooking, :share_living_room, :size
					
					unless @room.room_category.nil?
						json.category do
							json.id @room.room_category.id
							json.name @room.room_category.name
						end
					else
						json.category nil
					end
					
					unless @room.room_bathroom_type.nil?
						json.bathroom do
							json.id @room.room_bathroom_type.id
							json.name @room.room_bathroom_type.name 
						end
					else
						json.bathroom nil	
					end
					
					unless @room.lease_term.nil?
						json.lease_term do
							json.id @room.lease_term.id
							json.name @room.lease_term.name
						end
					else
						json.lease_term nil					
					end
					
					unless @room.furnishing.nil?
						json.furnishing do
							json.id @room.furnishing.id
							json.name @room.furnishing.name
						end
					else
						json.furnishing nil
					end
					
					unless @room.room_share_gender.nil?
						json.share_gender do
							json.id @room.room_share_gender.id
							json.name @room.room_share_gender.name
						end
					else
						json.share_gender nil
					end
					
					@location = @room.location
					unless @room.location.nil?
						json.address @location.address
						json.latitude @location.latitude
						json.longitude @location.longitude
					else
						json.address nil
						json.latitude nil
						json.longitude nil
					end
					
					@region = @room.region
					json.region do
						unless @region.nil?
							json.id @region.city_id
							json.name @region.city_name
						else
							json.id nil
							json.name nil
						end
					end
					
					@city = @room.city
					json.city do
						unless @city.nil?
							json.id @city.subcity_id
							json.name @city.subcity_name
						else
							json.id nil
							json.name nil
						end
					end	
				end
				if @room.location.present?
					json.badge_count Post.get_location_count @room.location.latitude, @room.location.latitude
				else
					json.badge_count 0
				end
			end
			
			@images = post.images
			unless @images.nil?
				json.images do
					json.array!(@images) do |image|
						json.id image.id
						json.original image.attachment.url(:original, false)
						json.thumb image.attachment.url(:thumb, false)
						json.small image.attachment.url(:small, false)
						json.medium image.attachment.url(:medium, false)
					end
				end
			else
				json.images nil
			end
			
			@floor_plan = post.floor_plan
			unless @floor_plan.nil?
				json.floor_plan do
					json.id @floor_plan.id
					json.original @floor_plan.attachment.url(:original, false)
					json.thumb @floor_plan.attachment.url(:thumb, false)
					json.small @floor_plan.attachment.url(:small, false)
					json.medium @floor_plan.attachment.url(:medium, false)
				end
			else
				json.floor_plan nil		
			end
			
			@video = post.video
			unless @video.nil?
				json.video do
					json.id @video.id
					json.path @video.attachment.url(:original, false)
				end
			else
				json.video nil		
			end
			
			json.created_at date_format(post.created_at)
			json.updated_at date_format(post.updated_at)
			
		end
	end
end
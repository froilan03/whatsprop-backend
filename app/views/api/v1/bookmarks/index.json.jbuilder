json.result true
json.code 200
json.data do
	json.array!(@bookmarks) do |bookmark|
		json.bookmark do
			json.id bookmark.id
			
			@post = bookmark.post
		
			json.post do
				json.id @post.id
				json.title @post.title
				json.description @post.description
				json.status @post.status
				json.expiry_date date_format(@post.expiry_date)
				json.bookmark do
					if params[:user_id].present?
						b = Bookmark.get_bookmarked params[:user_id], @post.id
						json.id b.first.nil? ? nil : b.first.id 
						json.value b.first.nil? ? "0" : "1"
					else
						json.id nil
						json.value "0"
					end			
				end
				
				json.user do
					json.extract! @post.user, :id, :first_name, :last_name, :email, :username, :mobile
				end
				
				@property = @post.property
				unless @property.nil?
					json.property do
						json.extract! @property, :id, :name, :price, :psf, :size, :number_of_bed, :number_of_bath, :year_constructed, :developer, :features, :amenities
						json.category @property.property_category.name unless @property.property_category.nil?
						json.type @property.property_type.name unless @property.property_type.nil?
						json.tenure @property.property_tenure.name unless @property.property_tenure.nil?
						json.lease_term @property.lease_term.name unless @property.lease_term.nil?
						json.furnishing @property.furnishing.name unless @property.furnishing.nil?
						
						@location = @property.location
						unless @property.location.nil?
							json.address @location.address
							json.latitude @location.latitude
							json.longitude @location.longitude
						else
							json.address nil
							json.latitude nil
							json.longitude nil
						end
						
						@region = @property.region
						json.region do
							unless @region.nil?
								json.id @region.city_id
								json.name @region.city_name
							else
								json.id nil
								json.name nil
							end
						end
						
						@city = @property.city
						json.city do
							unless @city.nil?
								json.id @city.subcity_id
								json.name @city.subcity_name
							else
								json.id nil
								json.name nil
							end
						end
						
					end
				end	
				
				@room = @post.room
				unless @room.nil?
					json.room do
						json.extract! @room, :id, :price, :cooking, :share_living_room, :size
						json.category @room.room_category.name unless @room.room_category.nil?
						json.bathroom_type @room.room_bathroom_type.name unless @room.room_bathroom_type.nil?
						json.lease_term @room.lease_term.name unless @room.lease_term.nil?
						json.furnishing @room.furnishing.name unless @room.furnishing.nil?
						json.share_gender @room.room_share_gender.name unless @room.room_share_gender.nil?
						
						@location = @room.location
						unless @room.location.nil?
							json.address @location.address
							json.latitude @location.latitude
							json.longitude @location.longitude
						else
							json.address nil
							json.latitude nil
							json.longitude nil
						end
						
						@region = @room.region
						json.region do
							unless @region.nil?
								json.id @region.city_id
								json.name @region.city_name
							else
								json.id nil
								json.name nil
							end
						end
						
						@city = @room.city
						json.city do
							unless @city.nil?
								json.id @city.subcity_id
								json.name @city.subcity_name
							else
								json.id nil
								json.name nil
							end
						end
						
					end
				end
				
				@images = @post.images
				unless @images.nil?
					json.images do
						json.array!(@images) do |image|
							json.id image.id
							json.original image.attachment.url(:original, false)
							json.thumb image.attachment.url(:thumb, false)
							json.small image.attachment.url(:small, false)
							json.medium image.attachment.url(:medium, false)
						end
					end
				else
					json.images []
				end
				
				@floor_plan = @post.floor_plan
				unless @floor_plan.nil?
					json.floor_plan do
						json.id @floor_plan.id
						json.original @floor_plan.attachment.url(:original, false)
						json.thumb @floor_plan.attachment.url(:thumb, false)
						json.small @floor_plan.attachment.url(:small, false)
						json.medium @floor_plan.attachment.url(:medium, false)
					end
				else
					json.floor_plan nil		
				end
				
				@video = @post.video
				unless @video.nil?
					json.video do
						json.id @video.id
						json.path @video.attachment.url(:original, false)
					end
				else
					json.video nil		
				end	
				
				json.created_at date_format(@post.created_at)
				json.updated_at date_format(@post.updated_at)
				
			end
			
			json.user do
				json.extract! bookmark.user, :id, :username, :email, :first_name, :last_name, :mobile
			end			
			
			json.created_at date_format bookmark.created_at
			json.updated_at date_format bookmark.updated_at
			
		end
	end
end
json.result true
json.code 200
json.data do
	json.extract! @announcement, :id, :title, :description
	json.created_at date_format @announcement.created_at
	json.updated_at date_format @announcement.updated_at
end
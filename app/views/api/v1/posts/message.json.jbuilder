json.result true
json.code 200
json.data do
	json.message do
			json.id @message.id
			@sender = User.find @message.sender_id
			json.sender do
				json.extract! @sender, :id, :first_name, :last_name, :email, :username, :mobile
				json.avatar_url @sender.avatar.url(:thumb, false)
			end
			@recipient = User.find @message.recipient_id
			json.recipient do
				json.extract! @recipient, :id, :first_name, :last_name, :email, :username, :mobile
				json.avatar_url @recipient.avatar.url(:thumb, false)
			end
			@msg_type = @sender.id.to_s == @user_id.to_s ? "Sent":"Received"
			json.type @msg_type
			json.message @message.body
			json.date @message.created_at
				
	end
end

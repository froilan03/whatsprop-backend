json.result true
json.code 200
json.data do
	json.array!(@posts) do |post|
		json.post do
			json.uid post.id
			json.title post.title
			json.description post.description
			json.created_at post.created_at
			json.user do
				json.extract! post.user, :id, :first_name, :last_name, :email, :username, :mobile
				json.avatar_url post.user.avatar.url(:thumb, false)
			end
		end
	end
end
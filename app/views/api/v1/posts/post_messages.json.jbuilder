json.result true
json.code 200
json.data do
	json.array!(@posts) do |post|
		
        json.post do
			json.id post.id
			json.title post.title
			
			@property = post.property
			unless @property.nil?
				@price = @property.price
			end	
			
			@room = post.room
			unless @room.nil?
				@price = @room.price
			end
			
			json.price @price
			@images = post.images
			unless @images.nil?
				json.images do
					json.array!(@images) do |image|
						json.id image.id
						json.original image.attachment.url(:original, false)
						json.thumb image.attachment.url(:thumb, false)
						json.small image.attachment.url(:small, false)
						json.medium image.attachment.url(:medium, false)
					end
				end
			else
				json.images []
			end
			
			@conversations = Message.message_posts(post.id)
            unless @conversations.nil?
				json.conversations do
					json.array!(@conversations) do |conv|
						#logger.info "checking if not in conversation: #{ @user_id.to_s != conv.sender_id.to_s and @user_id.to_s != conv.recipient_id.to_s}"
						next if @user_id.to_s != conv.sender_id.to_s and @user_id.to_s != conv.recipient_id.to_s
						
						if @user_id.to_s == conv.sender_id.to_s
							@current_user = User.find conv.recipient_id
						else
							@current_user = User.find conv.sender_id
						end
						
						json.conversation_with do
							json.extract! @current_user, :id, :first_name, :last_name, :email, :username, :mobile
							json.confirmed_at date_format(@current_user.confirmed_at)
							json.avatar_url @current_user.avatar.url(:thumb, false)
						end
						
						@messages = Message.between(conv.post_id, conv.sender_id, conv.recipient_id, @message_id)
						json.messages do
							json.array!(@messages) do |msg|
								if @user_id.to_s == msg.sender_id.to_s
									next if msg.sender_status
								else # recipient in the message
									next if msg.recipient_status
								end
								
								@msg_type = msg.sender_id.to_s == @user_id.to_s ? "Sent":"Received"
								json.id msg.id
								json.status msg.status
								#json.sender_status msg.sender_status
								#json.recipient_status msg.recipient_status
								json.type @msg_type
								json.message msg.body
								json.date date_format(msg.created_at)
							end
						end
 
					end
				end
			else
				json.conversations nil
            end
		end	
	end
end

json.result true
json.code 200
json.data do
	json.messages do
		json.array!(@message_info) do |msg|
			json.id msg.id
			@sender = User.find msg.sender_id
			json.sender do
				json.extract! @sender, :id, :first_name, :last_name, :email, :username, :mobile
				json.avatar_url @sender.avatar.url(:thumb, false)
			end
			@recipient = User.find msg.recipient_id
			json.recipient do
				json.extract! @recipient, :id, :first_name, :last_name, :email, :username, :mobile
				json.avatar_url @recipient.avatar.url(:thumb, false)
			end
			json.message do
				@msg_type = @sender.id.to_s == @user_id.to_s ? "Sent":"Received"
				json.type @msg_type
				json.message msg.body
				json.date msg.created_at
			end

		end
	end
end

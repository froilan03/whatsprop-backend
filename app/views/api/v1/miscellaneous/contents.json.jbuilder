json.result true
json.code 200
json.data do
	json.array!(@lists) do |item|
		json.id item.id
		json.name item.name
		
		if item.respond_to? :description
			json.description item.description
		end
		
		if item.respond_to? :code_type
			json.code_type item.code_type
		end	
	end
end
json.array!(@room_leases) do |room_lease|
  json.extract! room_lease, :id, :name, :description
  json.url room_lease_url(room_lease, format: :json)
end

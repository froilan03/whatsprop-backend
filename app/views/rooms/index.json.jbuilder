json.array!(@rooms) do |room|
  json.extract! room, :id, :price, :cooking, :share_living_room, :location_id, :property_type_id, :room_furnishing_id, :room_bath_id, :room_category_id, :room_lease_id
  json.url room_url(room, format: :json)
end

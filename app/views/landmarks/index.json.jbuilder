json.array!(@landmarks) do |landmark|
  json.extract! landmark, :id, :name, :description
  json.url landmark_url(landmark, format: :json)
end

json.array!(@property_psfs) do |property_psf|
  json.extract! property_psf, :id, :name, :description
  json.url property_psf_url(property_psf, format: :json)
end

json.array!(@users) do |user|
  json.extract! user, :id, :username, :first_name, :last_name, :email, :avatar_file_name, :avatar_content_type
  json.url user_url(user, format: :json)
end

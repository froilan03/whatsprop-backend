json.extract! @user, :id, :username, :first_name, :last_name, :email, :avatar_file_name, :avatar_content_type, :created_at, :updated_at

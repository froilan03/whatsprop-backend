json.array!(@servicing_agents) do |servicing_agent|
  json.extract! servicing_agent, :id, :title, :description, :price, :user_id
  json.url servicing_agent_url(servicing_agent, format: :json)
end

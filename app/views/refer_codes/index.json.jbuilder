json.array!(@refer_codes) do |refer_code|
  json.extract! refer_code, :id, :name, :code_type
  json.url refer_code_url(refer_code, format: :json)
end

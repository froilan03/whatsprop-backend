class Setting < ActiveRecord::Base
  validates :variable, uniqueness: true, presence: true
  validates :value, presence: true
  
  scope :find_value_of, ->(var){
    self.find_by_variable(var).value
  }
end
class Region < ActiveRecord::Base
  self.table_name = 'ph_cities'
  
  validates :city_name, uniqueness: true, presence: true
  
  has_many :cities, foreign_key: "city_id"
end
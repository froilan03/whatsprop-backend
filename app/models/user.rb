class User < ActiveRecord::Base
  has_one :agent_info
  has_one :account_credit
  
  belongs_to :user_role
  belongs_to :refer_code
  has_many :posts
  has_many :servicing_agents, :dependent => :destroy
  has_many :bookmarks
  has_many :app_shares
  has_many :post_views
  has_many :messages
  
  has_many :conversations, :foreign_key => :sender_id
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :timeoutable
         
  before_save :ensure_authentication_token  
  
  has_attached_file :avatar, styles: { medium: ["200x200>", :png], thumb: ["80x00>", :png] },
                              default_url: "/assets/missing_:style.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  
  #validates_presence_of :username
  
  include DeletableAttachment
  
  accepts_nested_attributes_for :agent_info
  
  scope :without_user, ->(user){ self.where.not(id: user.id)}
  
  def fullname 
    fullname = first_name.to_s.concat(" ").concat(last_name.to_s).concat(" - ").concat("<#{email}>")
  end
    
  # devise override active_for_authentication?
  def active_for_authentication?
    super && !self.access_locked?
  end
  
  def inactive_message 
    if self.access_locked? 
      :blocked 
    else 
      super 
    end 
  end
         
  def ensure_authentication_token
    if authentication_token.blank? 
      self.authentication_token = generate_authentication_token
    end
  end   
  
  def is_admin?
    self.user_role.name == 'Administrator'
  end
  
  def is_agent?
    self.user_role.name == 'Agent'
  end
  
  def is_non_agent?
    self.user_role.name == 'Non-Agent'
  end
  
  private
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end  
end

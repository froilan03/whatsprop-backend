class AgentInfo < ActiveRecord::Base
  self.table_name = 'agent_info'
  belongs_to :user
  
  validates :reg_no, presence: true, :if => lambda {self.user.user_role.name == "Agent"}
  validates :reg_period_start, presence: true, :if => lambda {self.user.user_role.name == "Agent"}
  validates :reg_period_end, presence: true, :if => lambda {self.user.user_role.name == "Agent"}
  validates :estate_agent_name, presence: true, :if => lambda {self.user.user_role.name == "Agent"}
  validates :estate_agent_license_number, presence: true, :if => lambda {self.user.user_role.name == "Agent"}
  
end
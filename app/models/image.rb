class Image < PostContentGallery
  has_attached_file :attachment, 
                    styles: { small: ["150x150>", :png], medium: ["300x300>", :png], thumb: ["50x50>", :png] }, 
                    default_url: "/assets/missing_:style.png"
                    
  validates_attachment :attachment, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  
  include Rails.application.routes.url_helpers
  
  def to_jq_upload
    {
    files: [
      {
        id:   read_attribute(:id),
        name: read_attribute(:attachment_file_name),
        size: read_attribute(:attachment_file_size),
        type: read_attribute(:attachment_content_type),
        thumbnail_url: attachment.url(:small),
        url: attachment.url(:original),
        delete_url: image_path(self),
        delete_type: "DELETE"
      }
    ]
  }
  end
                    
end
class PropertySize < ActiveRecord::Base
  self.table_name = 'property_size'
  
  validates :name, uniqueness: true, presence: true
  validates :range_from, :range_to, presence: true
  
  scope :get_size, ->(size_id){
    s = find_by(id: size_id)
    s.range_from..s.range_to
  }
end

class UserActivityLog < ActiveRecord::Base
  belongs_to :user
  

  def self.save_log(uid, evt)
    record = where(user_id:uid, event_name_ended:nil).first
    if record.nil?
       self.create event_name_started:evt, user_id:uid, started_at:Time.now
    else
      ended_at = Time.now
      record.update_columns(event_name_ended:evt, ended_at:ended_at, dwell_time: ended_at - record.started_at) 
    end
  end

  scope :avg_to_hour, ->()  do
    if average_dwell_time.empty?
      0
    else
      avg_seconds = average_dwell_time.to_i
      [avg_seconds / 3600, avg_seconds / 60 % 60, avg_seconds % 60].map { |t| t.to_s.rjust(2,'0') }.join(':')  
    end
  end  

  scope :average_dwell_time , ->() do
   average(:dwell_time)
  end
end

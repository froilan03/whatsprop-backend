class AccountCredit < ActiveRecord::Base
  belongs_to :user
  
  serialize :notification_params, Hash
   
  def paypal_url(return_url, amount)
    values = {
        business: "olavidezfroilan@yahoo.com",
        cmd: "_xclick",
        upload: 1,
        return: "#{Rails.application.secrets.app_host}#{return_url}",
        invoice: id,
        amount: amount,
        item_name: 'WhatsProp Credit',
        item_number: user.id,
        quantity: '1',
        notify_url: "#{Rails.application.secrets.app_host}/web_account/credits/hook"
    }
    
    "https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query
  end

end

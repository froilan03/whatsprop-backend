class Announcement < ActiveRecord::Base
  default_scope { order(updated_at: :desc)}
  
  validates_presence_of :title
  validates_presence_of :description
end

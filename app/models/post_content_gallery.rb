class PostContentGallery < ActiveRecord::Base
  self.table_name = 'post_content_gallery'
  belongs_to :assetable, :polymorphic => true
  belongs_to :content_type
  
  
  #has_attached_file :data, styles: { medium: ["300x300>", :png], thumb: ["50x50>", :png] },
  #                            default_url: "/assets/missing_:style.png"
  
  #validates_attachment_content_type :data, :content_type => /\Aimage\/.*\Z/
end
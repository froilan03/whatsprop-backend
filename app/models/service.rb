class Service < ActiveRecord::Base
  belongs_to :user
  
  validates_presence_of :description, :price, :user_id
  
  scope :all_with_filter, ->(sort_date, sort_price){
    service = self.all
    if !sort_date.blank?
      service.order(updated_at: sort_date.to_sym)
    elsif !sort_price.blank?
      service.order(price: sort_price.to_sym)
    else
      service.order(created_at: :desc)
    end    
  }
  
  scope :my_services, ->(id, sort_date, sort_price){
    service = self.where(user_id: id)
    if !sort_date.blank?
      service.order(updated_at: sort_date.to_sym)
    elsif !sort_price.blank?
      service.order(price: sort_price.to_sym)
    else
      service.order(created_at: :desc)
    end    
  }
  
end

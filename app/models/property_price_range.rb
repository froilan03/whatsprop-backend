class PropertyPriceRange < ActiveRecord::Base
  self.table_name = 'property_price_range'
  
  validates :name, uniqueness: true, presence: true
  validates :range_from, :range_to, presence: true
  
  scope :get_price_range, ->(p_ids){
    price_ids = p_ids.split(",")
    ranges = []
    price_ids.each do |price_id|
      p = find_by(id: price_id)  
      ranges.push p.range_from..p.range_to
    end
    ranges
  }
  
end

class PropertyCategory < ActiveRecord::Base
  self.table_name = 'property_category'
  
  validates :name, uniqueness: true, presence: true
  
  scope :get_category_by, -> (name){
    find_by(name: name)
  }
end

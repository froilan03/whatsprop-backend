class PostMapping < ActiveRecord::Base
  self.table_name = 'post_mapping'
  belongs_to :property, foreign_key: 'data_id'
  belongs_to :post
end

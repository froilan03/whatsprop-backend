class Property < ActiveRecord::Base
  self.table_name = 'property'
  
  belongs_to :post, :touch => true
  belongs_to :property_type
  belongs_to :property_tenure
  belongs_to :property_category
  belongs_to :location #, :dependent => :destroy
  belongs_to :lease_term
  belongs_to :furnishing
  
  belongs_to :region, foreign_key: "city_id"
  belongs_to :city, foreign_key: "subcity_id" 
  
  attr_accessor :postal_code
  
  validates :number_of_bed, presence: true
  validates :number_of_bath, presence: true
  validates :price, presence: true
  validates :size, presence: true
  
  validates :year_constructed, presence: true, :if => lambda {self.property_category.name == "SALE"}
  #validates :developer, presence: true, :if => lambda {self.property_category.name == "SALE"}
  validates :psf, presence: true, :if => lambda {self.property_category.name == "SALE"}
  
  #validates :features, presence: true
  #validates :amenities, presence: true
end
class Furnishing < ActiveRecord::Base
  has_one :property
  has_one :room
  
  validates :name, uniqueness: true, presence: true
end
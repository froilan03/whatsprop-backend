class Post < ActiveRecord::Base
  require 'will_paginate/array'
  
  belongs_to :user
  #belongs_to :post_content_gallery
  attr_accessor :post_type, :latitude, :longitude
  
  has_one :property, :dependent => :destroy
  has_one :room, :dependent => :destroy
  has_one :video, :as => :assetable, :class_name => "Video", :dependent => :destroy
  has_one :floor_plan, :as => :assetable, :class_name => "FloorPlan", :dependent => :destroy
  has_many :images, :as => :assetable, :class_name => "Image", :dependent => :destroy

  has_many :bookmarks, :dependent => :destroy
  has_many :post_views, :dependent => :destroy
  
  accepts_nested_attributes_for :property
  accepts_nested_attributes_for :room
  accepts_nested_attributes_for :images
  accepts_nested_attributes_for :floor_plan
  accepts_nested_attributes_for :video
  
  validates :title, uniqueness: true, presence: true
  validates :description, presence: true
  validates :expiry_date, presence: true
  
  after_update :update_property
  
  before_save :filter_post_description
  
  # updated property/room updated_at column once post is modified
  def update_property
    self.property.touch unless self.property.nil?
    self.room.touch unless self.room.nil?
  end
  
  def filter_post_description
    self.description.gsub!(/(\+?65)?\s?(((3|6)[0-9]{3}\s?[0-9]{4})|((7|8)[1-9][0-9]{2}\s?[0-9]{4})|((9)[0-8][0-9]{2}\s?[0-9]{4}))\b/, '')
    self.description.gsub!(/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/i, '')
    # (\+?65)?\s?(((3|6)[0-9]{3}\s?[0-9]{4})|((7|8)[1-9][0-9]{2}\s?[0-9]{4})|((9)[0-8][0-9]{2}\s?[0-9]{4}))\b
  end  
  
  scope :current_user_posts, -> (user_id){
    self.where(user_id: user_id)
  }

  scope :post_with_user_messages, -> (user_id, post_id){
    if !post_id.nil? or !post_id.blank?
      self.joins("LEFT JOIN messages ON messages.post_id = posts.id").where("(messages.sender_id = ? or recipient_id = ?) AND post_id=?", user_id, user_id, post_id).group(:post_id).order("messages.created_at")
    else
      self.joins("LEFT JOIN messages ON messages.post_id = posts.id").where("messages.sender_id = ? or recipient_id = ?", user_id, user_id).group(:post_id).order("messages.created_at")
    end       
  }
  
  scope :admin_posts, -> (){
    self.where(posted_by: 'admin')
  }
  
  scope :active, -> (){
    self.where(status: 'active')
  }
  
  scope :draft, -> (){
    self.where(status: 'draft')
  }
  
  scope :inactive, -> (){
    self.where(status: 'inactive')
  }
  
  scope :expired, -> (){
    self.where(status: 'expired')
  }
  
  scope :user_total_posts, ->(user_id){
    self.joins(:user).where(status: 'active', users: {id: user_id}).count  
  }
  
  scope :all_with_filter, -> (type, category, sort_date, sort_price, lat, long, region, city){
    if type.blank? or type.nil? # all active posts
     
      props = self.property_posts                         
      rooms = self.room_posts
      
      if !lat.blank? and !long.blank?
        props = props.where(location_id: Location.nearby(lat, long).map(&:id))
        rooms = rooms.where(location_id: Location.nearby(lat, long).map(&:id))
        properties = props.concat(rooms)
      elsif !region.blank? and !city.blank?
        props = props.where(city_id: region, subcity_id: city)
        rooms = rooms.where(city_id: region, subcity_id: city)
        properties = props.concat(rooms)
      else  
        properties = props.concat(rooms)
      end
      
      properties = (sort_price == 'asc') ? properties.sort_by(&:price) : properties.sort_by(&:price).reverse unless sort_price.blank?
      properties = (sort_date == 'asc') ? properties.sort_by(&:updated_at) : properties.sort_by(&:updated_at).reverse unless sort_date.blank?  
    
    else  # all property or room active posts
    
      if type == 'PROP' or type == 'property' # all property posts
        properties = self.property_posts.where(property_category_id: PropertyCategory.get_category_by(category).id)
      elsif type == 'ROOM' # all room posts
        properties = self.room_posts.where(room_category_id: RoomCategory.get_category_by(category).id)
      end
      
      if !properties.nil?
        
        if !lat.blank? and !long.blank?
          properties = properties.where(location_id: Location.nearby(lat, long).map(&:id))
        elsif !region.blank? and !city.blank?
          properties = properties.where(city_id: region, subcity_id: city)
        end
      
        #properties = properties.where(location_id: Location.nearby(lat, long).map(&:id)) if !lat.blank? and !long.blank?
        
        properties = properties.order(price: sort_price.to_sym) if !sort_price.blank?
        properties = properties.order(updated_at: sort_date.to_sym) if !sort_date.blank?
      end
    end
    
    posts = []
    properties.each { |prop| posts << prop.post }
   
    posts
  }

  scope :get_by_location, -> (lat, long){
    props = self.property_posts.where(location_id: Location.find_by_lat_long(lat, long).map(&:id))
    rooms = self.room_posts.where(location_id: Location.find_by_lat_long(lat, long).map(&:id))
    
    properties = props.concat(rooms)
    
    posts = []
    properties.each { |prop| posts << prop.post } 
   
    posts
  }
  
  scope :get_location_count, -> (lat, long){
    props_count = self.property_posts.where(location_id: Location.find_by_lat_long(lat, long).map(&:id)).count
    rooms_count = self.room_posts.where(location_id: Location.find_by_lat_long(lat, long).map(&:id)).count
    props_count + rooms_count
  }

  scope :get_location_count_with_radius, -> (lat, long){
    props_count = self.property_posts.where(location_id: Location.nearby(lat, long).map(&:id)).count
    rooms_count = self.room_posts.where(location_id: Location.nearby(lat, long).map(&:id)).count

    #props_count = self.property_posts.where(location_id: Location.find_by_lat_long(lat, long).map(&:id)).count
    #rooms_count = self.room_posts.where(location_id: Location.find_by_lat_long(lat, long).map(&:id)).count
    props_count + rooms_count
  }
  
  
  scope :property_posts, -> (){
    Property.joins(:post).where(posts: { status: 'active' })
  }
  
  scope :room_posts, -> (){
    Room.joins(:post).where(posts: { status: 'active'})
  }
  
  scope :find_location, ->(lat, long){
    self.where(location_id: Location.nearby(lat, long).map(&:id))
  }
  
  def self.check_expiring_record
    active_posts = self.active
    active_posts.each do |post|
      if post.expiry_date == Date.today + 1
        UserMailer.send_post_expiry_notification(post).deliver  
      end  
      
      if Date.today > post.expiry_date
        post.update_column(:status, 'expired')
      end  
    end
  end
    
end
class FloorPlan < PostContentGallery
  has_attached_file :attachment, 
                    styles: { small: ["150x150>", :png], medium: ["300x300>", :png], thumb: ["50x50>", :png] }, 
                    default_url: "/assets/missing_:style.png"
                    
  validates_attachment :attachment, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }                  
end
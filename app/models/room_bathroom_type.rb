class RoomBathroomType < ActiveRecord::Base
  validates :name, uniqueness: true, presence: true
  
  has_one :room
end
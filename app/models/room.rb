class Room < ActiveRecord::Base
  
  belongs_to :post, touch: true
  belongs_to :property_type
  belongs_to :room_bathroom_type
  belongs_to :room_category
  belongs_to :room_share_gender
  belongs_to :location #, :dependent => :destroy
  belongs_to :lease_term
  belongs_to :furnishing
  
  belongs_to :region, foreign_key: "city_id"
  belongs_to :city, foreign_key: "subcity_id"
  
  attr_accessor :postal_code
  
  validates :price, presence: true
  validates :size, presence: true, :if => lambda {self.room_category.name == "RENT"}
  validates :cooking, presence: true
  validates :share_living_room, presence: true
end
class Bookmark < ActiveRecord::Base
  default_scope -> { order(created_at: :desc) }
  
  belongs_to :user
  belongs_to :post
  
  validates_presence_of :post_id
  validates_presence_of :user_id
  
  scope :is_bookmarked, -> (uid, pid){
    b = self.where("user_id = ? and post_id = ?", uid, pid).first
    return b.nil? ? "0" : "1"
  }
  
  scope :get_bookmarked, -> (uid, pid){
    b = self.where("user_id = ? and post_id = ?", uid, pid).limit(1)
    return b
  }
end
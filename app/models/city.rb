class City < ActiveRecord::Base
  self.table_name = 'ph_subcities'
  
  validates :subcity_name, uniqueness: true, presence: true
  validates :city_id, presence: true
  
  belongs_to :region
end
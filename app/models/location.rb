class Location < ActiveRecord::Base
  belongs_to :located, :polymorphic => true
  
  has_one :property
  has_one :room
  
  #geocoded_by :address

  # the callback to set longitude and latitude
  #after_validation :geocode, :if => :address_changed?
  
  reverse_geocoded_by :latitude, :longitude
  #after_validation :reverse_geocode, if: ->(obj){ (obj.latitude.present? and obj.latitude_changed?) or (obj.longitude.present? and obj.longitude_changed?) }
  
  scope :find_or_create, -> (lat, long, addr){
    self.find_or_create_by(latitude: lat, longitude:long) do |loc|
       loc.address = addr
    end
  }
   
  scope :nearby, -> (lat, long){
    self.near([lat, long], 2, :units => :km)
  }
  
  scope :find_by_lat_long, -> (lat, long){
    self.where(latitude: lat, longitude: long)
  }
end
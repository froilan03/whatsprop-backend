class PropertyTenure < ActiveRecord::Base
  self.table_name = 'property_tenure'
  
  validates :name, uniqueness: true, presence: true
end
class Message < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user
 
  validates_presence_of :body, :sender_id, :recipient_id, :post_id

  scope :involving, -> (user_id) do
    where("sender_id =? OR recipient_id =?",user_id, user_id)
  end

  scope :between, -> (post_id, sender_id, recipient_id, message_id) do
    if !message_id.nil? or !message_id.blank?
      where("((sender_id =? AND recipient_id =?) OR (sender_id =? AND recipient_id =?)) AND post_id=? AND id=?", sender_id, recipient_id, recipient_id, sender_id, post_id, message_id).order(:created_at)
    else
      where("((sender_id =? AND recipient_id =?) OR (sender_id =? AND recipient_id =?)) AND post_id=?", sender_id, recipient_id, recipient_id, sender_id, post_id).order(:created_at)
    end
  end

  scope :total_post_message, -> (post_id) do
    where("post_id =?", post_id).count
  end
  
  scope :total_unread_message, -> (user_id) do
    where("recipient_id =? AND status=?", user_id, "NEW").count
  end

  scope :message_posts, -> (post_id) do
    where("post_id =?", post_id).group('LEAST(sender_id, recipient_id), GREATEST(sender_id, recipient_id)').order(:created_at)
  end

  scope :message_info, -> (user_id, post_id) do
    where("(sender_id =? or recipient_id=?) and post_id =?", user_id, user_id, post_id)
  end
end 
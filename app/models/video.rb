class Video < PostContentGallery
  has_attached_file :attachment, default_url: "/assets/missing_:style.png"
  
  validates_attachment :attachment, content_type: { content_type: /^video\/(mp4|quicktime)/ }
end
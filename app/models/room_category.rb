class RoomCategory < ActiveRecord::Base
  self.table_name = 'room_category'
  
  validates :name, uniqueness: true, presence: true
  
  scope :get_category_by, -> (name){
    self.find_by(name: name)
  }
end
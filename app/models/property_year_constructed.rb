class PropertyYearConstructed < ActiveRecord::Base
  self.table_name = 'property_year_constructed'
  
  validates :name, uniqueness: true, presence: true
end

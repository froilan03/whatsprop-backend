class ServicingAgent < ActiveRecord::Base
  belongs_to :user
  
  validates_presence_of :title
  validates_presence_of :description
  
  scope :all_with_filter, ->(sort_date, sort_price){
    if !sort_date.blank?
      self.all.order(updated_at: sort_date.to_sym)
    elsif !sort_price.blank?
      self.all.order(price: sort_price.to_sym)
    else
      self.all.order(title: :asc)
    end    
  }
  
  scope :my_services, ->(id, sort_date, sort_price){
    service = self.where(user_id: id)
    if !sort_date.blank?
      service.order(updated_at: sort_date.to_sym)
    elsif !sort_price.blank?
      service.order(price: sort_price.to_sym)
    else
      service.order(created_at: :desc)
    end    
  }
end

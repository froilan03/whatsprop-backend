class RoomShareGender < ActiveRecord::Base
  self.table_name = 'room_share_gender'
  
  validates :name, uniqueness: true, presence: true
end
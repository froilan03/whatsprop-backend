class PropertyPsf < ActiveRecord::Base
  self.table_name = 'property_psf'
  
  validates :name, uniqueness: true, presence: true
end

class ReferCode < ActiveRecord::Base
    validates :name, uniqueness: true, presence: true
    validates :code_type, uniqueness: true, presence: true
    
    scope :validate_code, ->(param){ self.where(code_type: param)}
end
module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def resource_class
    devise_mapping.to
  end
  
  def date_format d
    d.strftime("%m/%d/%Y %I:%M:%S %p") unless d.nil?
  end
  
  def is_active?(controller)
    'active' if controller.include?(controller_name)
  end
  
  def account_status(user)
    if user.access_locked?
      content_tag(:span, 'Blocked', :class=> 'label label-danger', :title => 'Account Blocked')
    elsif !user.confirmed?
      content_tag(:span, 'Unconfirmed', :class=> 'label label-default', :title => 'Account Not Verified')
    elsif user.last_sign_in_at.present?
      content_tag(:span, 'Active', :class=> 'label label-success', :title => 'Account Active')
    else
      content_tag(:span, 'Inactive', :class=> 'label label-warning', :title => 'Account Inactive')  
    end
  end
  
  def post_status(status)
    if status == 'draft'
      content_tag(:span, 'Draft', :class=> 'label label-warning', :title => 'Draft')
    elsif status == 'active'
      content_tag(:span, 'Active', :class=> 'label label-success', :title => 'Active')
    elsif status == 'inactive'
      content_tag(:span, 'Inactive', :class=> 'label label-default', :title => 'Inactive')
    elsif status == 'expired'
      content_tag(:span, 'Expired', :class=> 'label label-danger', :title => 'Expired')  
    end
  end
  
  def post_category(post)
    category = nil
    unless post.property.nil?
      category = "#{post.property.class.name} for #{post.property.property_category.name.capitalize}"
    end
    
    unless post.room.nil?
      #category = "#{post.room.class.name} for #{post.room.room_category.name.capitalize}"
      category = "#{post.room.class.name} "
      if post.room.room_category.name.downcase == 'rent'
        category << "for #{post.room.room_category.name.capitalize}" 
      else  
        category << "Sharing"
      end
    end
    category
  end
  
  def post_property_type(post)
    unless post.property.nil?
      return post.property.property_type.name
    end
    unless post.room.nil?
      return post.room.property_type.name
    end    
  end
  
  def get_post_category_name(post_type, category_type)
    unless post_type.nil?
      "#{post_type}_category".camelize.constantize.get_category_by(category_type)
    end
  end
  
  def share_type_label type
    if type == 'email'
      content_tag(:span, type.capitalize, :class=> 'label label-success')
    else
      content_tag(:span, type.capitalize, :class=> 'label label-info')  
    end
    
  end
  
  def new_button
    content_tag(:i, '', :class=>'fa fa-plus')+content_tag(:span, ' New')
  end
  
  def add_host_prefix(url)
    path = ActionController::Base.asset_host + url
    path
  end
end
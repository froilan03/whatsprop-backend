class RoomBathsController < ApplicationController
  before_action :set_room_bath, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @room_baths = RoomBath.all
    respond_with(@room_baths)
  end

  def show
    respond_with(@room_bath)
  end

  def new
    @room_bath = RoomBath.new
    respond_with(@room_bath)
  end

  def edit
  end

  def create
    @room_bath = RoomBath.new(room_bath_params)
    @room_bath.save
    respond_with(@room_bath)
  end

  def update
    @room_bath.update(room_bath_params)
    respond_with(@room_bath)
  end

  def destroy
    @room_bath.destroy
    respond_with(@room_bath)
  end

  private
    def set_room_bath
      @room_bath = RoomBath.find(params[:id])
    end

    def room_bath_params
      params.require(:room_bath).permit(:name, :description)
    end
end

class RoomShareGendersController < ApplicationController
  before_action :set_room_share_gender, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @room_share_genders = RoomShareGender.all
    respond_with(@room_share_genders)
  end

  def show
    respond_with(@room_share_gender)
  end

  def new
    @room_share_gender = RoomShareGender.new
    respond_with(@room_share_gender)
  end

  def edit
  end

  def create
    @room_share_gender = RoomShareGender.new(room_share_gender_params)
    @room_share_gender.save
    respond_with(@room_share_gender)
  end

  def update
    @room_share_gender.update(room_share_gender_params)
    respond_with(@room_share_gender)
  end

  def destroy
    @room_share_gender.destroy
    respond_with(@room_share_gender)
  end

  private
    def set_room_share_gender
      @room_share_gender = RoomShareGender.find(params[:id])
    end

    def room_share_gender_params
      params.require(:room_share_gender).permit(:name, :description)
    end
end

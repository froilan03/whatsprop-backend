class AdminPostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @posts = Post.admin_posts
    @posts = @posts.paginate(:page => params[:page], :per_page => item_per_page)
    
    respond_with(@posts)
  end

  def show
    respond_with(@post)
  end

  def new
    @post = Post.new
    @post.build_property
    @post.build_room
    @post.build_floor_plan
    @post.build_video
    
    respond_with(@post)
  end

  def edit
    @images = Image.all
    if @post.floor_plan.nil?
      @post.build_floor_plan
    end
    
    if @post.video.nil?
      @post.build_video
    end
    
    if @post.property.present?
      @latitude = @post.property.location.latitude unless @post.property.location.nil?
      @longitude = @post.property.location.longitude unless @post.property.location.nil? 
    end
      
    if @post.room.present?
      @latitude = @post.room.location.latitude unless @post.room.location.nil?
      @longitude = @post.room.location.longitude unless @post.room.location.nil? 
    end  
      
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @images.map{|image| image.to_jq_upload } }
    end
  end

  def create
    @post_type = params[:post][:post_type]
    @post = Post.new(send("post_#{@post_type}_params"))
    @post.user_id = current_user.id
    @post.posted_by = 'admin'
    
    property_or_room = @post.send(@post_type)
    property_or_room.location = Location.find_or_create(params[:post][:latitude], params[:post][:longitude])
    
    respond_to do |format|
      if @post.save
        if @post.video.present?
          @post.update_column(:status, 'active')
        end
        format.html { redirect_to admin_posts_path }
      else
        @post.build_floor_plan
        @post.build_video
        @post.build_property
        @post.build_room
    
        format.html { render action: 'new' }
      end
    end
  end

  def update
    post_type = params[:post][:post_type]
    respond_to do |format|
      if !params[:post][:latitude].blank? and !params[:post][:longitude].blank?
        property_or_room = @post.send(post_type)
        property_or_room.location = Location.find_or_create(params[:post][:latitude], params[:post][:longitude])
      end
        
      if @post.update(send("post_#{post_type}_params"))
        if @post.status != 'active' && (@post.video.present? or @post.images.present?)
          @post.update_column(:status, 'active')
        end
        format.html { redirect_to admin_posts_path }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  def destroy
    @post.destroy
    redirect_to admin_posts_path
  end

  private
    def set_post
      @post = Post.find(params[:id])
    end
    
    def post_property_params
      params.require(:post).permit(:id, :title, :description, :expiry_date, :post_type, :latitude, :longitude, property_attributes: [:id, :name, :number_of_bed, :number_of_bath, :year_constructed, :price, :size, :developer, :features, :amenities, :psf, :property_type_id, :property_tenure_id, :lease_term_id, :furnishing_id, :property_category_id, :city_id, :subcity_id ], floor_plan_attributes: [:id, :attachment], video_attributes: [:id, :attachment])
    end
    
    def post_room_params
      params.require(:post).permit(:id, :title, :description, :expiry_date, :post_type, :latitude, :longitude, room_attributes: [:id, :price, :cooking, :share_living_room, :size, :room_share_gender_id, :room_bathroom_type_id, :furnishing_id, :lease_term_id, :property_type_id, :room_category_id, :city_id, :subcity_id], floor_plan_attributes: [:id, :attachment], video_attributes: [:id, :attachment])
    end
end

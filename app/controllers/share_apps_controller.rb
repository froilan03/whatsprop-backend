class ShareAppsController < ApplicationController
  before_action :authenticate_user!

  respond_to :html

  def index
    @app_shares = AppShare.all
    respond_with(@app_shares)
  end
  
  def destroy
    @app_share = AppShare.find(params[:id])
    @app_share.destroy
    respond_with(@app_share)
  end

end

class SessionsController < Devise::SessionsController
  
  # override create method to add additional params for flash message, :username
  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in, :username => resource.username) if is_flashing_format?
    sign_in(resource_name, resource)
    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for(resource)
  end  
end
class PropertyTypesController < ApplicationController
  before_action :set_property_type, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @property_types = PropertyType.all
    respond_with(@property_types)
  end

  def show
    respond_with(@property_type)
  end

  def new
    @property_type = PropertyType.new
    respond_with(@property_type)
  end

  def edit
  end

  def create
    @property_type = PropertyType.new(property_type_params)
    @property_type.save
    respond_with(@property_type)
  end

  def update
    @property_type.update(property_type_params)
    respond_with(@property_type)
  end

  def destroy
    @property_type.destroy
    respond_with(@property_type)
  end

  private
    def set_property_type
      @property_type = PropertyType.find(params[:id])
    end

    def property_type_params
      params.require(:property_type).permit(:name, :description)
    end
end

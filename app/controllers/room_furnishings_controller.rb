class RoomFurnishingsController < ApplicationController
  before_action :set_room_furnishing, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @room_furnishings = RoomFurnishing.all
    respond_with(@room_furnishings)
  end

  def show
    respond_with(@room_furnishing)
  end

  def new
    @room_furnishing = RoomFurnishing.new
    respond_with(@room_furnishing)
  end

  def edit
  end

  def create
    @room_furnishing = RoomFurnishing.new(room_furnishing_params)
    @room_furnishing.save
    respond_with(@room_furnishing)
  end

  def update
    @room_furnishing.update(room_furnishing_params)
    respond_with(@room_furnishing)
  end

  def destroy
    @room_furnishing.destroy
    respond_with(@room_furnishing)
  end

  private
    def set_room_furnishing
      @room_furnishing = RoomFurnishing.find(params[:id])
    end

    def room_furnishing_params
      params.require(:room_furnishing).permit(:name, :description)
    end
end

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  before_action :restrict_access
  
  helper_method :item_per_page
  
  layout 'whatsprop'
  
  protected
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:first_name, :last_name, :username, :email, :password, :password_confirmation, :current_password) }
  end
    
  def restrict_access
    if current_user 
      if !current_user.is_admin? and !['web_accounts', 'account_credits', 'sessions'].include?(controller_name)
        redirect_to(credits_path)
      elsif current_user.is_admin? and ['web_accounts', 'account_credits'].include?(controller_name)
        redirect_to(home_path, notice: "You are not authorized to perform that action")
      end  
    end
  end

  # override devise default sign in landing page
  def after_sign_in_path_for(resource)
    if resource.is_admin?
      if session["user_return_to"] 
        flash.now[:notice] = nil
        session["user_return_to"]
      else
        home_path # admin home page  
      end
    else 
      if session["user_return_to"]
        flash.now[:notice] = nil
        session["user_return_to"]
      else
        credits_path # web account page   
    end
    end  
  end
  
  # override devise default sign out landing page
  def after_sign_out_path_for(resource)
     new_user_session_path
  end
  
  def item_per_page
    30
  end
  
end

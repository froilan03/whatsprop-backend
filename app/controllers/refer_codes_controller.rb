class ReferCodesController < ApplicationController
  before_action :set_refer_code, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @refer_codes = ReferCode.all
    respond_with(@refer_codes)
  end

  def show
    respond_with(@refer_code)
  end

  def new
    @refer_code = ReferCode.new
    respond_with(@refer_code)
  end

  def edit
  end

  def create
    @refer_code = ReferCode.new(refer_code_params)
    @refer_code.save
    respond_with(@refer_code)
  end

  def update
    @refer_code.update(refer_code_params)
    respond_with(@refer_code)
  end

  def destroy
    @refer_code.destroy
    respond_with(@refer_code)
  end

  private
    def set_refer_code
      @refer_code = ReferCode.find(params[:id])
    end

    def refer_code_params
      params.require(:refer_code).permit(:name, :code_type)
    end
end

class RoomBathroomTypesController < ApplicationController
  before_action :set_room_bathroom_type, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @room_baths = RoomBathroomType.all
    respond_with(@room_baths)
  end

  def show
    respond_with(@room_bath)
  end

  def new
    @room_bath = RoomBathroomType.new
    respond_with(@room_bath)
  end

  def edit
  end

  def create
    @room_bath = RoomBathroomType.new(room_bath_params)
    @room_bath.save
    respond_with(@room_bath)
  end

  def update
    @room_bath.update(room_bath_params)
    respond_with(@room_bath)
  end

  def destroy
    @room_bath.destroy
    respond_with(@room_bath)
  end

  private
    def set_room_bathroom_type
      @room_bath = RoomBathroomType.find(params[:id])
    end

    def room_bath_params
      params.require(:room_bathroom_type).permit(:name, :description)
    end
end

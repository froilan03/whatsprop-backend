class ServicingAgentsController < ApplicationController
  before_action :set_servicing_agent, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @servicing_agents = ServicingAgent.all
    respond_with(@servicing_agents)
  end

  def show
    respond_with(@servicing_agent)
  end

  def new
    @servicing_agent = ServicingAgent.new
    respond_with(@servicing_agent)
  end

  def edit
  end

  def create
    @servicing_agent = ServicingAgent.new(servicing_agent_params)
    @servicing_agent.save
    respond_with(@servicing_agent)
  end

  def update
    @servicing_agent.update(servicing_agent_params)
    respond_with(@servicing_agent)
  end

  def destroy
    @servicing_agent.destroy
    respond_with(@servicing_agent)
  end

  private
    def set_servicing_agent
      @servicing_agent = ServicingAgent.find(params[:id])
    end

    def servicing_agent_params
      params.require(:servicing_agent).permit(:title, :description, :price, :user_id)
    end
end

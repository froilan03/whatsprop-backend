class LeaseTermsController < ApplicationController
  before_action :set_lease_term, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @lease_terms = LeaseTerm.all
    respond_with(@lease_terms)
  end

  def show
    respond_with(@lease_term)
  end

  def new
    @lease_term = LeaseTerm.new
    respond_with(@lease_term)
  end

  def edit
  end

  def create
    @lease_term = LeaseTerm.new(lease_term_params)
    @lease_term.save
    respond_with(@lease_term)
  end

  def update
    @lease_term.update(lease_term_params)
    respond_with(@lease_term)
  end

  def destroy
    @lease_term.destroy
    respond_with(@lease_term)
  end

  private
    def set_lease_term
      @lease_term = LeaseTerm.find(params[:id])
    end

    def lease_term_params
      params.require(:lease_term).permit(:name, :description)
    end
end

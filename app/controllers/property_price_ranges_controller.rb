class PropertyPriceRangesController < ApplicationController
  before_action :set_property_price_range, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @property_price_ranges = PropertyPriceRange.all
    respond_with(@property_price_ranges)
  end

  def show
    respond_with(@property_price_range)
  end

  def new
    @property_price_range = PropertyPriceRange.new
    respond_with(@property_price_range)
  end

  def edit
  end

  def create
    @property_price_range = PropertyPriceRange.new(property_price_range_params)
    @property_price_range.save
    respond_with(@property_price_range)
  end

  def update
    @property_price_range.update(property_price_range_params)
    respond_with(@property_price_range)
  end

  def destroy
    @property_price_range.destroy
    respond_with(@property_price_range)
  end

  private
    def set_property_price_range
      @property_price_range = PropertyPriceRange.find(params[:id])
    end

    def property_price_range_params
      params.require(:property_price_range).permit(:name, :description, :range_from, :range_to)
    end
end

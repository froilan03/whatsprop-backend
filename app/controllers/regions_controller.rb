class RegionsController < ApplicationController
  before_action :set_region, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @regions = Region.order(:city_name)
    @regions = @regions.paginate(:page => params[:page], :per_page => item_per_page)
    
    respond_with(@regions)
  end

  def show
    respond_with(@region)
  end

  def new
    @region = Region.new
    respond_with(@region)
  end

  def edit
  end

  def create
    @region = Region.new(region_params)
    @region.save
    respond_with(@region)
  end

  def update
    @region.update(region_params)
    respond_with(@region)
  end

  def destroy
    @region.destroy
    respond_with(@region)
  end

  private
    def set_region
      @region = Region.find(params[:id])
    end

    def region_params
      params.require(:region).permit(:city_name)
    end
end

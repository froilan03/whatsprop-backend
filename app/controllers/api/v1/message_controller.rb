class Api::V1::MessagesController < Api::V1::BaseController
  #before_filter :authenticate_user!
  skip_before_action :authenticate_user_from_token!, only: [:create, :updatestatus]
  
  def create
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.build(message_params)
    @message.user_id = current_user.id
    
    payload = @message.attributes
    payload[:user] = current_user.attributes
    
    if @message.save
      Pusher.trigger("private-channel" + conversation_path(@conversation), 'send_message', payload)
      render :text => "sent"
    else
      render :text => "failed"  
    end
  end
  
  def updatestatus
    @message = Message.find(params[:id])
    @message.status = params[:status]
    if @message.save
      render :index
    else
      render status:422, json: { result:false, code:422, errors:@message.errors, data:{} }
    end
    
  end
  
  def delete_message
    @message = Message.find(params[:id])
    @type = params[:type]
    if @type.nil? or @type.blank?
      render status:422, json: { result:false, code:422, errors:{message: 'Invalid request, missing type param'}, data:{} }
    else
      if @type == 'sender'
        logger.info "?>>>> sender"
        @message.sender_status = 1  
      end
      if @type == 'recipient'  
        logger.info "?>>>> recipient"
        @message.recipient_status = 1  
      end
      
      logger.info "?>>>> sender_status"+@message.sender_status.to_s
      logger.info "?>>>> recipient_status"+@message.recipient_status.to_s
      # check if both flag are 1, if yes destroy the message
      if @message.sender_status && @message.recipient_status
        logger.info "?>>>> equal.."
        if @message.destroy
          render json: { result:true, code:200, errors: {}, data:{}, message: "Success" }  
        else
          render status:422, json: { result:false, code:422, errors:@message.errors, data:{} }
        end
      else # update the status
        if @message.save
          render json: { result:true, code:200, errors: {}, data:{}, message: "Success" }  
        else
          render status:422, json: { result:false, code:422, errors:@message.errors, data:{} }
        end
      end
    end
     
  end
  
  def get_unread
    @total_unread = Message.total_unread_message(params[:user_id])
    render status:200, json: { result:true, code:200, data:{ count: @total_unread } }
  end

  private
  def message_params
    params.require(:message).permit(:body)
  end
end 
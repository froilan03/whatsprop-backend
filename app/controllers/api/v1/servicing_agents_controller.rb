class Api::V1::ServicingAgentsController < Api::V1::BaseController
  before_action :set_servicing_agent, only: [:update, :destroy]

  skip_before_action :authenticate_user_from_token!, only: [:index, :info]
  
  respond_to :json

  def index
    @servicing_agents = ServicingAgent.all_with_filter(params[:sort_date], params[:sort_price]).paginate(:page => params[:page], :per_page => params[:item_per_page])
    render :index
  end
  
  def my_services
    @servicing_agents = ServicingAgent.my_services(params[:id], params[:sort_date], params[:sort_price]).paginate(:page => params[:page], :per_page => params[:item_per_page])
    render :index
  end

  def info
    @servicing_agent = ServicingAgent.find(params[:id])
    render :info
  end
  
  def create
    @servicing_agent = ServicingAgent.new(servicing_agent_params)
    if @servicing_agent.save
      render :info
    else
      render status:422, json: { result:false, code:422, errors: @servicing_agent.errors, data:{} }    
    end
  end

  def update
    if @servicing_agent.update(servicing_agent_params)
      render :info
    else
      render status:422, json: { result:false, code:422, errors: @servicing_agent.errors, data:{} }  
    end
  end

  def destroy
    if @servicing_agent.destroy
      render json: { result:true, code:200, errors: {}, data:{}, message: "Success" }
    else
      render status:422, json: { result:false, code:422, errors: @servicing_agent.errors, data:{} }    
    end
    
  end

  private
    def set_servicing_agent
      @servicing_agent = ServicingAgent.find(params[:id])
    end

    def servicing_agent_params
      params.require(:servicing_agent).permit(:title, :description, :price, :user_id)
    end
end

class Api::V1::ChatController < Api::V1::BaseController
  skip_before_action :authenticate_user_from_token!
  
  respond_to :json
  
  def webhooks
    logger.info ">>>>> chat! webhooks"
    render json: { message: 'success'}
  end
  
  def hello_world
    logger.info ">>>>> chat! hello world"
    Pusher.trigger('test_channel', 'my_event', {
      message: params[:msg]
    })
    
    render json: { message: 'success'}
  end

  def auth
    if params[:user_id]
      user = User.find(params[:user_id])
      auth = Pusher[params[:channel_name]].authenticate(params[:socket_id], :user_id => user.id, :user => user.attributes)
      render :json => auth
    else
      render :text => "Not authorized", :status => '403'
    end
  end
  
  def email_message
    message = Message.new
    message.sender_id = params[:sender_id]
  	message.recipient_id = params[:recipient_id]
  	
  	msg = params[:message]
  	sanitized_msg = msg.gsub(/%0A+/, '\n').strip
  	
  	message.body = sanitized_msg
  	message.post_id = params[:post_id]
  	
  	if message.save
  		UserMailer.email_chat_message(params[:sender_id], params[:recipient_id], sanitized_msg).deliver
  	end
    
    render json: { message: 'success' }
  end
  
  def my_messages	
    if params[:message_type] == 'inbox'
  		@messages = Message.where(recipient_id: params[:user_id])
  	elsif params[:message_type] == 'sent'
  		@messages = Message.where(sender_id: params[:user_id])
  	end
	
    render :my_messages
  end
  
  def message_info
    @message = Message.find params[:id]
    render :message_info
  end
  
  def delete_message
  	@message = Message.find_by_id params[:message_id]
  	@message.destroy 
  	
  	render json: { message: 'success' }
  end
  
  private
  
  def conversation_params
    params.permit(:sender_id, :recipient_id)
  end
  
  def message_params
    params.require(:message).permit(:body)
  end
end

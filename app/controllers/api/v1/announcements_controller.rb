class Api::V1::AnnouncementsController < Api::V1::BaseController
  skip_before_action :authenticate_user_from_token!, only: [:index, :info]
  
  respond_to :json

  def index
    @announcements = Announcement.all
    render :index
  end

  def info
    @announcement = Announcement.find(params[:id])
    render :info
  end
end
class Api::V1::RegionsController < Api::V1::BaseController
  
  skip_before_action :authenticate_user_from_token!, only: [:index]
  
  respond_to :json

  def index
    begin
      @regions = Region.all
      render :index
    rescue
     render status:500, json: { result:false, code:500, errors: { message: "Error retrieving regions & cities" }, data:[] }  
    end
  end
  
end

class Api::V1::RegistrationsController < Devise::RegistrationsController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  
  before_action :ensure_params_exist
  
  def create
    @user = User.new(email:params[:name], password:params[:password], password_confirmation:params[:password_confirmation])
    #user_role = UserRole.find_by_name('Guest')
    # Agent, or Guest
    user_role = UserRole.find_by_name(params[:user_role]) 
    
    if  params[:refer_code].present?
      rc = ReferCode.find_by_code_type(params[:refer_code])
      @user.refer_code = rc unless rc.nil?
    end
     
    @user.user_role = user_role unless user_role.nil?
    
    if @user.save
      render :create
      
      #@user.send_confirmation_instructions
      #UserMailer.welcome_email(@user).deliver
      
      return
    else
      warden.custom_failure!
      render status: 422, json: {
               result:false,
               code:422,
               message: { errors: @user.errors},
               data:{}
      }
    end
  end
  
  private
  def ensure_params_exist
    if params[:name].present? && params[:password].present?
      
      if params[:refer_code].present?
        code = ReferCode.validate_code(params[:refer_code])
        if code.empty?
          errors = {refer_code: ["Invalid refer code."]}
        else
          return  
        end
      else
        return  
      end
      
    elsif params[:name].blank?
      errors = {email: ["can't be blank"]} 
    elsif params[:password].blank?
      errors = {password: ["can't be blank"]}    
    else
      errors = {email: ["can't be blank"], password: ["can't be blank"]}
    end
    
    render status: 422, json: { result:false, code:422, message: {errors: errors}, data:{} } 
  end
end
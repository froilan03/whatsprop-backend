class Api::V1::BaseController < ActionController::Base
  respond_to :json
  
  before_action :authenticate_user_from_token!
  #before_filter :authorize_admin!, :except => [:index, :show]
  #before_filter :check_rate_limit
  
  #rescue_from Exception, with: :application_error

  private
  def authenticate_user_from_token!
    @current_user = User.find_by_authentication_token(params[:auth_token])
    unless @current_user
      render :json => {result:"false",  code:401, errors: { message: "Invalid authentication token." }, data:{}}, :status => 401
    end
  end

  def current_user
    @current_user
  end

  def authorize_admin!
    if !@current_user.admin?
      error = { :error => "You must be an admin to do that." }
      warden.custom_failure!
      render params[:format].to_sym => error, :status => 401
    end
  end

  def authenticate_user_from_token
    user_email = params[:email].presence
    user = user_email && User.find_by_email(email)

    # Notice how we use Devise.secure_compare to compare the token
    # in the database with the token given in the params, mitigating
    # timing attacks.
    if user && Devise.secure_compare(user.authentication_token, params[:auth_token])
      sign_in user, store: false
    end
  end
  
  def application_error
    render status:500, json: {result:false, code:500, errors: {messge: 'Application error occurred while processing your request. Please try again'}}
  end
end

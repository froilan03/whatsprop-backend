class Api::V1::AnalyticsController < Api::V1::BaseController
  respond_to :json
  
  skip_before_action :authenticate_user_from_token!
  
  def user_activity_log
    UserActivityLog.save_log(params[:user_id], params[:activity])
    render json: {result:true, code:200, message: 'Success'}
  end
  
  def guest_activity_log
    GuestActivityLog.create(action: params[:activity], created_at:Time.now)
    render json: {result:true, code:200, message: 'Success'}
  end

end
class Api::V1::ShareController < Api::V1::BaseController
  respond_to :json
  
  skip_before_action :authenticate_user_from_token!
  
  def share_app
    @type = params[:type]
    @email = params[:email]
    @user_id = params[:user_id]
    
    @app_share = AppShare.new(user_id: @user_id, email: @email, created_at:Time.now, share_type: @type)
    if @type == 'email'
      if @app_share.save
        UserMailer.share_app_email(@email).deliver
        render json: {result:true, code:200, message: 'Success', errors:{}}
      else
        render status:422, json: {result:false, code:422, errors: @app_share.errors}
      end  
    else # facebook
      if @app_share.save
        render json: {result:true, code:200, message: 'Success', errors:{}}
      else
        render status:422, json: {result:false, code:422, errors: @app_share.errors}    
      end
    end
  end
  
  def share_post
    @type = params[:type]
    @email = params[:email]
    @user_id = params[:user_id]
    @post_id = params[:post_id]
    
    @post_share = PostShare.new(user_id: @user_id, post_id: @post_id, email: @email, share_type: @type)
    if @type == 'email'
      if @post_share.save
        UserMailer.share_post_email(@email, @post_id).deliver
        render json: {result:true, code:200, message: 'Success', errors:{}}
      else
        render status:422, json: {result:false, code:422, errors: @post_share.errors}  
      end
    else # facebook
      if @post_share.save
        render json: {result:true, code:200, message: 'Success', errors:{}}
      else
        render status:422, json: {result:false, code:422, errors: @post_share.errors}    
      end 
    end
  end
end

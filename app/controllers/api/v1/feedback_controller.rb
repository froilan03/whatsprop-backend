class Api::V1::FeedbackController < Api::V1::BaseController
  respond_to :json
  
  skip_before_action :authenticate_user_from_token!
  
  def leave_feedback
    if params[:user_id].nil? or params[:user_id].blank? or params[:user_id].empty? 
      render status:422, json: {result:false, code:422, message:'User id is required'}  
    else
      @feedback = Feedback.new(user_id: params[:user_id], message: params[:message], created_at: Time.now)
      if @feedback.save
        FeedbackMailer.send_feedback(@feedback.user_id, @feedback.message).deliver
        FeedbackMailer.forward_to_marketing(@feedback.message).deliver
      end
      render json: {result:true, code:200, message:'Success'}
    end
  end
end
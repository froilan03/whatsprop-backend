class Api::V1::SearchController < Api::V1::BaseController
  respond_to :json
  
  def index
    location = params[:location]
    if !location.nil?
      location = location.gsub!("(", "")
      location = location.gsub!(")", "")
      lat = location.split("|").first 
      long = location.split("|").last
    end
    
    type = params[:type] # type of post -  property or room
    category = params[:mode] # mode of property/room - SALE, RENT, SHARING
    prop_type_id = params[:prop_type_id] # selected property type id, e.g. HBD, Condo
    region_id = params[:region_id]
    city_id = params[:city_id]
    price_from = params[:price_from]
    price_to = params[:price_to]
    size_from = params[:size_from]
    size_to = params[:size_to]
    lease_term_id = params[:lease_term_id]
    furnishing_id = params[:furnishing_id]
    
    # for property posts additional filter parameters
    no_of_beds = params[:no_of_bed]# number of bed
    no_of_baths = params[:no_of_bath] # number of bath
    tenure_id = params[:tenure_id] # selected tenure id
    
    # for room posts additional filter parameters
    gender_id = params[:gender_id] # selected gender id, male, female, any
    room_bathroom_type_id = params[:room_bathroom_type_id]
    can_cook = params[:can_cook]
    share_living_room = params[:share_living_room]
    
    @posts = []
    if type.to_sym == :property
      p = Property.joins(:post).where(posts: { status: 'active' })
      unless p.blank?
        p = p.where(property_category_id: PropertyCategory.find_by_name(category).id) if category.present?
        p = p.where(property_type_id: prop_type_id) if prop_type_id.present?
        
        if no_of_beds.present?
          p = no_of_beds.include?("+") ? p.where("number_of_bed >= ?", no_of_beds.to_i) : p.where("number_of_bed <= ?", no_of_beds)
        end
         
        if no_of_baths.present?
          p = no_of_baths.include?("+") ? p.where("number_of_bath >= ?", no_of_baths.to_i) : p.where("number_of_bath <= ?", no_of_baths)
        end 
         
        p = p.where(property_tenure_id: tenure_id) if tenure_id.present?
        p = p.where(lease_term_id: lease_term_id) if lease_term_id.present?
        p = p.where(furnishing_id: furnishing_id) if furnishing_id.present?
        
        p = p.where(price: price_from.to_i..price_to.to_i) if price_from.present? && price_to.present?
        p = p.where(size: size_from.to_i..size_to.to_i) if size_from.present? && size_to.present?
        
        p = p.where(city_id: region_id) if region_id.present?
        p = p.where(subcity_id: city_id) if city_id.present?
        
        if (lat.present? and long.present?) and (lat != 'undefined' and long != 'undefined')
          p = p.where(location_id: Location.nearby(lat, long).map(&:id)) 
        end
        
        p.each { |prop| @posts << prop.post }
      end               
    elsif type.to_sym == :room
      r = Room.joins(:post).where(posts: { status: 'active' })
      unless r.blank?
        r = r.where(room_category_id: RoomCategory.find_by_name(category).id) if category.present?
        r = r.where(property_type_id: prop_type_id) if prop_type_id.present?
        r = r.where(room_share_gender_id: gender_id) if gender_id.present?
        r = r.where(lease_term_id: lease_term_id) if lease_term_id.present?
        r = r.where(furnishing_id: furnishing_id) if furnishing_id.present?
        
        r = r.where(room_bathroom_type_id: room_bathroom_type_id) if room_bathroom_type_id.present?
        r = r.where(cooking: can_cook) if can_cook.present?
        r = r.where(share_living_room: share_living_room) if share_living_room.present?
        
        r = r.where(city_id: region_id) if region_id.present?
        r = r.where(subcity_id: city_id) if city_id.present?
        
        r = r.where(price: price_from.to_i..price_to.to_i) if price_from.present? && price_to.present?
        r = r.where(size: size_from.to_i..size_to.to_i) if size_from.present? && size_to.present?
        
        if (lat.present? and long.present?) and (lat != 'undefined' and long != 'undefined')
          r = r.where(location_id: Location.nearby(lat, long).map(&:id)) 
        end
        
        r.each { |prop| @posts << prop.post }
      end
    end
    
    render :index
  end
end

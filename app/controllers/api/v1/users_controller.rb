class Api::V1::UsersController < Api::V1::BaseController
  before_action :set_user, only: [:update, :upload_avatar, :remove_avatar, :change_password, :user_info]
  respond_to :json
  
  def update
    if @user.update(user_params)
      render :update
    else
      render status:422, json: { result:false, code:422, errors: @user.errors, data:{} }  
    end
  end
 
  def change_password
    if @user.update(user_password_params)
      render :update
    else
      render status:422, json: { result:false, code:422, errors: @user.errors, data:{} }  
    end
  end
  # Request payload
  # { "image": 
      #{
      #  filename: "original_filename.jpg",
      #  file_data: "<Base64Encoded data>",
      #  content_type: "<image content type>" # e.g. image/jpeg
      #}
  # }
  
  def upload_avatar
    content = ContentUploaderService.new(params[:image][:file_data], params[:image][:filename], params[:image][:content_type]).upload
    
    #avatar = ImageUploaderService.new(params[:image]).upload
    #@user.avatar = params[:avatar]
    
    if @user.update(avatar: content)
      render :update
    else
      render status:422, json: { result:false, code:422, errors: { message: "Error uploading avatar"} }
    end    
  end 
  
  def remove_avatar
    if @user.update(delete_avatar: params[:delete_avatar])
      render json: { result:true, code:200, errors:{}, message: "Success" }
    else
      render status:422, json: { result:false, code:422, errors: { message: "Error deleting avatar" } }
    end    
  end
  
  def user_info
    render :user_info
  end
  
  def available_roles
    begin
      @user_roles = UserRole.all
      render :available_roles
    rescue
      render status:500, json: { result:false, code:500, errors: { message: "Error retrieving user roles" }, data:[] }  
    end      
  end
  
  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_blocked_params
    params.require(:user).permit(:access_locked)
  end
  
  def user_password_params
    params.require(:user).permit(:password, :password_confirmation)
  end
  
  def user_params
    #params.require(:user).permit(:first_name, :last_name, :email, :username, :password, :password_confirmation, :avatar, :delete_avatar)
    params.require(:user).permit(:first_name, :last_name, :email, :username, :mobile)
  end
end
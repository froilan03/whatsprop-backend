class Api::V1::BookmarksController < Api::V1::BaseController
  before_action :set_bookmark, only: [:show, :update, :destroy]
  
  skip_before_action :authenticate_user_from_token!, only: [:index, :show, :create, :update, :destroy]
  
  respond_to :json

  def index
     @bookmarks = Bookmark.where(user_id: params[:user_id])
     render :index
  end
  
  def create
    @bookmark = Bookmark.new(bookmark_params)
    if @bookmark.save
      render :info
    else
      render status:422, json: { result:false, code:422, errors: @bookmark.errors, data:{} }   
    end
  end

  def destroy
    if @bookmark.destroy
      render json: { result:true, code:200, errors: {}, data:{}, message: "Success" }
    else
      render status:500, json: { result:false, code:500, errors: { message: "Error deleting bookmark."}, data:{} }    
    end
  end

  private
    def set_bookmark
      @bookmark = Bookmark.find(params[:id])
    end

    def bookmark_params
      params.require(:bookmark).permit(:post_id, :user_id)
    end
end

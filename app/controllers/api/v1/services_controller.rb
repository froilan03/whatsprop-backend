class Api::V1::ServicesController < Api::V1::BaseController
  before_action :set_service, only: [:update, :destroy]

  skip_before_action :authenticate_user_from_token!, only: [:index, :info]
  
  respond_to :json

  def index
    @services = Service.all_with_filter(params[:sort_date], params[:sort_price]).paginate(:page => params[:page], :per_page => params[:item_per_page])
    render :index
  end

  def my_services
    @services = Service.my_services(params[:id], params[:sort_date], params[:sort_price]).paginate(:page => params[:page], :per_page => params[:item_per_page])
    render :index
  end
  
  def info
    @service = Service.find(params[:id])
    render :info
  end
  
  def create
    @service = Service.new(service_params)
    if @service.save
      render :info
    else
      render status:422, json: { result:false, code:422, errors: @service.errors, data:{} }    
    end
  end

  def update
    if @service.update(service_params)
      render :info
    else
      render status:422, json: { result:false, code:422, errors: @service.errors, data:{} }  
    end
  end

  def destroy
    if @service.destroy
      render json: { result:true, code:200, errors: {}, data:{}, message: "Success" }
    else
      render status:422, json: { result:false, code:422, errors: @service.errors, data:{} }    
    end
    
  end

  private
    def set_service
      @service = Service.find(params[:id])
    end

    def service_params
      params.require(:service).permit(:description, :price, :user_id)
    end
end

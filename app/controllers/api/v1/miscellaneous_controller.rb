class Api::V1::MiscellaneousController < Api::V1::BaseController
  respond_to :json

  skip_before_action :authenticate_user_from_token!
  
  # type values
  # -- landmarks
  # -- property_categories
  # -- property_price_ranges
  # -- property_psfs
  # -- property_sizes
  # -- property_tenures
  # -- property_types
  # -- property_year_constructed
  # -- room_categories
  # -- room_baths
  # -- room_furnishings
  # -- room_leases
  # -- room_share_genders
  # -- refer_codes
    
  def index
    @lists = params[:type].classify.constantize.all
    render :contents  
  end
end
class Api::V1::PostsController < Api::V1::BaseController
  before_action :set_post, only: [:info, :all_by_location, :publish, :unpublish, :upload_asset, :destroy]
  
  skip_before_action :authenticate_user_from_token!, only: [:index, :info, :create, :update]
  
  respond_to :json

  def index
    @posts = Post.all_with_filter(params[:type],
                                   params[:mode],
                                   params[:sort_date], 
                                   params[:sort_price],  
                                   params[:latitude], 
                                   params[:longitude],
                                   params[:region_id],
                                   params[:city_id]).paginate(:page => params[:page], :per_page => params[:item_per_page])
    render :index
  end
  
  def get_by_location
    @posts = Post.get_by_location(params[:latitude], params[:longitude])
    
    render :index
  end
  
  def map_posts_listings
    @posts = Post.get_by_location(params[:latitude], params[:longitude])
    
    render :map_posts_listings
  end

  def get_post_count_by_location_with_radius
    @count = Post.get_location_count_with_radius(params[:latitude], params[:longitude])
    @lat = params[:latitude]
    @long = params[:longitude]

    render :get_post_count_by_location_with_radius
  end
  
  def info
    unless params[:user_id].nil?
      PostView.create(post_id: @post.id, user_id:params[:user_id])
    end  
    
    render :info  
  end
  
  def current_user_posts
    @posts = Post.current_user_posts(params[:user_id])
    render :index
  end

  def get_post_messages
    @post_id = params[:post_id]
    @message_id = params[:message_id]
    
    @user_id = params[:user_id]
    @posts = Post.post_with_user_messages(@user_id, @post_id)
    render :post_messages
  end

  def get_post_message_info
    @user_id = params[:user_id]
    @message_info = Message.message_info(@user_id, params[:post_id])
    render :post_message_info
  end

  def get_message 
    @user_id = params[:user_id]
    @message = Message.find(params[:message_id])
    render :message
  end
  
  def publish
    @post.status = 'active'
    
    current_total_post = Post.user_total_posts @post.user.id
    limit_post = Setting.find_value_of('limit_post')
    on_promotion = Setting.find_value_of('on_promotion')
    cost_per_post = Setting.find_value_of('cost_per_post')
    if current_total_post == limit_post
        render status:422, json: { result:false, code:422, errors: 'Post limit exceeded!' }
    else
      if on_promotion == 'no'
        @account_credit = @post.user.account_credit
        credit_amount = @account_credit.amount.to_f - cost_per_post.to_f
        #logger.info "Credit amount: #{credit_amount}"
        if credit_amount > 0
          if @post.save
              @account_credit = @post.user.account_credit
              @account_credit.update_attribute(:amount, credit_amount)
          
              UserMailer.send_credit_notification(@post).deliver
              render :info
          else
            render status:422, json: { result:false, code:422, errors:@post.errors }
          end  
        else
          render status:422, json: { result:false, code:422, errors: 'Insufficient credit amount.' }  
        end
      else
         if @post.save
            #UserMailer.send_credit_notification(@post).deliver
            render :info
          else
            render status:422, json: { result:false, code:422, errors:@post.errors }
          end  
      end
    end
      
  end
  
  def unpublish
    @post.status = 'inactive'
    if @post.save
      render :info
    else
      render status:422, json: { result:false, code:422, errors:@post.errors }
    end
  end
  
  def create
    @post = Post.new(post_params)
    post_type = params[:post_type]
    category_type = params[:category_type] # sale, rent, share
    
    model = post_type.camelize.constantize.new(send("#{post_type}_#{category_type.downcase}_params"))
	  @post.send("#{post_type}=".to_sym, model)
	  
	  post_property_room = @post.send(post_type)
	  post_property_room.send("#{post_type}_category=".to_sym, "#{post_type}_category".camelize.constantize.get_category_by(category_type))
	  
	  if !params[:latitude].nil? and !params[:longitude].nil?
	   post_property_room.location = Location.find_or_create(params[:latitude], params[:longitude], params[:address])
	  end
	  
    if @post.save
      render :info
    else
      render status:422, json: { result:false, code:422, errors: @post.errors, data:{} }    
    end    
  end
  
  def update
    post_type = params[:post_type]
    category_type = params[:category_type] # sale, rent, share
    
    @post = Post.find(params[:id])
    if @post.update(post_params)
      model = @post.send(post_type)
      
      model.send("#{post_type}_category=".to_sym, "#{post_type}_category".camelize.constantize.get_category_by(category_type))
      
      if !params[:latitude].nil? and !params[:longitude].nil?
        model.location = Location.find_or_create(params[:latitude], params[:longitude], params[:address])
      end
      
      model.update(send("#{post_type}_#{category_type.downcase}_params"))
      
      render :info
    else
      render status:422, json: { result:false, code:422, errors: @post.errors, data:{} }    
    end
    
  end
  
  # Request payload
  # { "content": 
      #{
      #  filename: "original_filename.jpg",
      #  file_data: "<Base64Encoded data>",
      #  content_type: "<image content type>" # e.g. image/jpeg
      #}
  # }
  def upload_asset
    @content_type = params[:asset_type]
    #@content_type = 'floor_plan'
    content = ContentUploaderService.new(params[:content][:file_data], params[:content][:filename], params[:content][:content_type]).upload
    model = @content_type.camelize.constantize.new(attachment: content)
    if @content_type == 'image'
      @post.images << model

    elsif @content_type == 'floor_plan'
      @post.floor_plan = model
       
    elsif @content_type == 'video'
      @post.video = model
    end
    
    logger.info ">>> saving #{@content_type}...."
    if @post.save
      logger.info ">>> #{@content_type} saved!"
      render :info
    else
      render status:422, json: { result:false, code:422, errors: { message: "Error uploading content"} }
    end    
  end 
  
  def destroy_asset
    content_type = params[:asset_type]
    @asset = content_type.camelize.constantize.find_by_id(params[:asset_id])
    
    if @asset.destroy
      render json: { result:true, code:200, errors:{}, message: "Success" }
    else
      render status:422, json: { result:false, code:422, errors: { message: "Error deleting content" } }
    end    
  end
  
  def destroy
    if @post.destroy
      render json: { result:true, code:200, errors: {}, data:{}, message: "Success" }
    else
      render status:422, json: { result:false, code:422, errors: @post.errors, data:{} }    
    end
  end

  private
    def set_post
      @post = Post.find(params[:post_id])
    end

    def post_params
      params.require(:post).permit(:title, :description, :status, :user_id, :expiry_date)
    end
    
    def property_sale_params
      params.require(:property).permit(:name, :number_of_bed, :number_of_bath, :year_constructed, :price, :size, :developer, :features, :amenities, :psf, :property_type_id, :property_tenure_id, :city_id, :subcity_id)
    end
    
    def property_rent_params
      params.require(:property).permit(:name, :number_of_bed, :number_of_bath, :year_constructed, :price, :size, :developer, :features, :amenities, :psf, :property_type_id, :lease_term_id, :furnishing_id, :city_id, :subcity_id)
    end
    
    def room_rent_params
      params.require(:room).permit(:price, :cooking, :share_living_room, :size, :room_bathroom_type_id, :furnishing_id, :lease_term_id, :property_type_id, :city_id, :subcity_id)
    end
    
    def room_share_params
      params.require(:room).permit(:price, :cooking, :share_living_room, :room_share_gender_id, :room_bathroom_type_id, :furnishing_id, :lease_term_id, :property_type_id, :city_id, :subcity_id)
    end
end

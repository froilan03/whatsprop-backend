class Api::V1::SessionsController < Devise::SessionsController
  prepend_before_filter :require_no_authentication, :only => [:create ]
  before_action :ensure_params_exist, :only => [:create ]
  
  skip_before_action :verify_authenticity_token
  skip_before_action :verify_signed_out_user, only: :destroy
  
  respond_to :json
  
  def create
    @user = User.find_for_database_authentication(email: params[:name])
    return invalid_login_attempt unless @user
    
    if @user.access_locked?
      render status:401, json: {result:false, code:401, errors: {message: "Your account is blocked!"}, data:{}} 
      return
    elsif !@user.confirmed?
      render status:401, json: {result:false, code:401, errors: {message: "Your account is not yet confirmed!"}, data:{}} 
      return
    elsif @user.valid_password?(params[:password]) && 
      sign_in("user", @user)
      @user.save
      
      render :create
      return
    end
    
    invalid_login_attempt
  end

  def destroy
    #resource = User.find_for_database_authentication(email: params[:user][:email])
    user_email = params[:name].presence
    user = user_email && User.find_by_email(user_email)

    # Notice how we use Devise.secure_compare to compare the token in the database with the token given in the params, mitigating timing attacks.
    if user && Devise.secure_compare(user.authentication_token, params[:auth_token])
      user.update_column(:authentication_token, nil)
      render json: { result:true, code:200, message: "Logout successful!" }
    else
      render json: { result:false, code:401, errors: { message: "Invalid user credential." } }, status: 401    
    end
    
  end

  protected
  def ensure_params_exist
    return if !params[:name].blank? && !params[:password].blank?
    
    if params[:name].blank?
      errors = {email: ["can't be blank"]} 
    elsif params[:password].blank?
      errors = {password: ["can't be blank"]}
    else
      errors = { email: ["can't be blank"], password: ["can't be blank"] }
    end
    
    render status: 422, json: {  result:false, code:422, message: {errors: errors}, data:{} } 
  end
  
  def invalid_login_attempt
    warden.custom_failure!
    render json: { result:false, code:401, errors: { message: "Invalid login credentials" }, data:{} }, status: 401
  end
end

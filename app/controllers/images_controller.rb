class ImagesController < ApplicationController
  def index
    @images = Image.all
    render :json => @images.collect { |p| p.to_jq_upload }.to_json
  end

  def new
    @image = Image.new # needed for form_for --> gets the path
  end

  def create
    post_id = params[:image][:post_id]
    @image = Image.new
    @image.attachment = params[:image][:attachment].shift
    @image.assetable_id = post_id
    @image.assetable_type = 'Post'
    if @image.save
      respond_to do |format|
        format.html { #(html response is for browsers using iframe sollution)
          render :json => [@image.to_jq_upload].to_json, :content_type => 'text/html', :layout => false
        }
        format.json {
          logger.info ">>>>>>>>>>>> result: #{[@image.to_jq_upload].to_json}"
          render(json: @image.to_jq_upload, content_type: request.format)
        }
      end
    else
      render :json => [{:error => "custom_failure"}], :status => 304
    end
  end

  def destroy
    @image = Image.find(params[:id])
    @image.destroy
    render :json => true
  end
end
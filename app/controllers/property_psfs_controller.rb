class PropertyPsfsController < ApplicationController
  before_action :set_property_psf, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @property_psfs = PropertyPsf.all
    respond_with(@property_psfs)
  end

  def show
    respond_with(@property_psf)
  end

  def new
    @property_psf = PropertyPsf.new
    respond_with(@property_psf)
  end

  def edit
  end

  def create
    @property_psf = PropertyPsf.new(property_psf_params)
    @property_psf.save
    respond_with(@property_psf)
  end

  def update
    @property_psf.update(property_psf_params)
    respond_with(@property_psf)
  end

  def destroy
    @property_psf.destroy
    respond_with(@property_psf)
  end

  private
    def set_property_psf
      @property_psf = PropertyPsf.find(params[:id])
    end

    def property_psf_params
      params.require(:property_psf).permit(:name, :description)
    end
end

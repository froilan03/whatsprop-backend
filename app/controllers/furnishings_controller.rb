class FurnishingsController < ApplicationController
  before_action :set_furnishing, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @furnishings = Furnishing.all
    respond_with(@furnishings)
  end

  def show
    respond_with(@furnishing)
  end

  def new
    @furnishing = Furnishing.new
    respond_with(@furnishing)
  end

  def edit
  end

  def create
    @furnishing = Furnishing.new(furnishing_params)
    @furnishing.save
    respond_with(@furnishing)
  end

  def update
    @furnishing.update(furnishing_params)
    respond_with(@furnishing)
  end

  def destroy
    @furnishing.destroy
    respond_with(@furnishing)
  end

  private
    def set_furnishing
      @furnishing = Furnishing.find(params[:id])
    end

    def furnishing_params
      params.require(:furnishing).permit(:name, :description)
    end
end

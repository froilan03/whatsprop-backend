class PropertySizesController < ApplicationController
  before_action :set_property_size, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @property_sizes = PropertySize.all
    respond_with(@property_sizes)
  end

  def show
    respond_with(@property_size)
  end

  def new
    @property_size = PropertySize.new
    respond_with(@property_size)
  end

  def edit
  end

  def create
    @property_size = PropertySize.new(property_size_params)
    @property_size.save
    respond_with(@property_size)
  end

  def update
    @property_size.update(property_size_params)
    respond_with(@property_size)
  end

  def destroy
    @property_size.destroy
    respond_with(@property_size)
  end

  private
    def set_property_size
      @property_size = PropertySize.find(params[:id])
    end

    def property_size_params
      params.require(:property_size).permit(:name, :description, :range_from, :range_to)
    end
end

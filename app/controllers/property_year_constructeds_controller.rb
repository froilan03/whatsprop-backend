class PropertyYearConstructedsController < ApplicationController
  before_action :set_property_year_constructed, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @property_year_constructeds = PropertyYearConstructed.all
    respond_with(@property_year_constructeds)
  end

  def show
    respond_with(@property_year_constructed)
  end

  def new
    @property_year_constructed = PropertyYearConstructed.new
    respond_with(@property_year_constructed)
  end

  def edit
  end

  def create
    @property_year_constructed = PropertyYearConstructed.new(property_year_constructed_params)
    @property_year_constructed.save
    respond_with(@property_year_constructed)
  end

  def update
    @property_year_constructed.update(property_year_constructed_params)
    respond_with(@property_year_constructed)
  end

  def destroy
    @property_year_constructed.destroy
    respond_with(@property_year_constructed)
  end

  private
    def set_property_year_constructed
      @property_year_constructed = PropertyYearConstructed.find(params[:id])
    end

    def property_year_constructed_params
      params.require(:property_year_constructed).permit(:name, :description)
    end
end

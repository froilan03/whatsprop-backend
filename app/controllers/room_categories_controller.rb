class RoomCategoriesController < ApplicationController
  before_action :set_room_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @room_categories = RoomCategory.all
    respond_with(@room_categories)
  end

  def show
    respond_with(@room_category)
  end

  def new
    @room_category = RoomCategory.new
    respond_with(@room_category)
  end

  def edit
  end

  def create
    @room_category = RoomCategory.new(room_category_params)
    @room_category.save
    respond_with(@room_category)
  end

  def update
    @room_category.update(room_category_params)
    respond_with(@room_category)
  end

  def destroy
    @room_category.destroy
    respond_with(@room_category)
  end

  private
    def set_room_category
      @room_category = RoomCategory.find(params[:id])
    end

    def room_category_params
      params.require(:room_category).permit(:name, :description)
    end
end

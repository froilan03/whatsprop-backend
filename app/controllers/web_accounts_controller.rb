class WebAccountsController < ApplicationController
  before_action :authenticate_user!, :except => [:hook]
  skip_before_filter :verify_authenticity_token
  
  layout 'web_account'
  def index
    if current_user.account_credit.nil?
      current_user.account_credit = AccountCredit.create(user_id: current_user.id, amount: 0)
    end
    
    @promotion = Setting.find_by_variable('on_promotion')
  end
  
  def edit_profile
    @user = current_user
  end
  
  def change_password
    @user = current_user
  end
  
  def hook
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]
    if status == "Completed"
      @credit = AccountCredit.find params[:invoice]
      
      total_amount = @credit.amount + params[:mc_gross].to_f
      @credit.update_attributes(amount: total_amount, notification_params: params, status: status, transaction_id: params[:txn_id], purchased_at: Time.now)
    end
    render nothing: true
  end
  
  def update
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
    @user = current_user
    if @user.update(user_params)
        redirect_to credits_path  
    else
        render 'web_accounts/edit_profile'  
    end
  end

  def update_password
    @user = current_user
    if @user.update(user_params)
        redirect_to credits_path  
    else
        render 'web_accounts/change_password'  
    end
  end
  
  private
    def user_params
      params.require(:user).permit(:username, :first_name, :last_name, :email, :avatar, :delete_avatar, :mobile, :password, :password_confirmation, agent_info_attributes: [:id, :reg_no, :reg_period_start, :reg_period_end, :estate_agent_name, :estate_agent_license_number])
    end
end
class AccountCreditsController < ApplicationController
  before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token, :only => [:payment]
  
  layout 'web_account'
  def index
    
  end

  def payment
    @amount = params[:amount] + ".00"
  end  
  
end 
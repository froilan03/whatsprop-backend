class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @users = User.without_user(current_user)
    @users = @users.paginate(:page => params[:page], :per_page => item_per_page)
    
    respond_with(@users)
  end

  def show
    respond_with(@user)
  end

  def new
    @user = User.new
    respond_with(@user)
  end

  def edit
  end

  def edit_profile
    @user = User.find(params[:user_id])
  end
  
  def create
    @user = User.new(user_params)
    
    if @user.save
      flash[:notice] = 'Successfully created'
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def update
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
    
    if @user.update(user_params)
      flash[:notice] = 'Successfully updated'
      if current_user.is_admin?
        redirect_to :action => 'index'
      else
        redirect_to credits_path  
      end
    else
      logger.info ">>>>>>>>>>> render!"
      #if @user.is_admin?
      #  render :action => 'edit'
      #else
        render 'web_accounts/edit_profile'  
      #end
    end
  end

  def destroy
    @user.destroy
    respond_with(@user)
  end
  
  def block
    @user = User.find(params[:user_id])
    @user.update(access_locked: 1)
    
    flash[:notice] = 'Successfully blocked'
    redirect_to :action => :index
  end
  
  def unblock
    @user = User.find(params[:user_id])
    @user.update(access_locked: 0)
    
    flash[:notice] = 'Successfully unblocked'
    redirect_to :action => :index
  end
  
  private
    def set_user
      @user = User.find(params[:id])
    end
    
    def user_params
      params.require(:user).permit(:username, :first_name, :last_name, :email, :avatar, :delete_avatar, :user_role_id, :password, :password_confirmation)
    end
end

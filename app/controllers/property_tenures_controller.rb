class PropertyTenuresController < ApplicationController
  before_action :set_property_tenure, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @property_tenures = PropertyTenure.all
    respond_with(@property_tenures)
  end

  def show
    respond_with(@property_tenure)
  end

  def new
    @property_tenure = PropertyTenure.new
    respond_with(@property_tenure)
  end

  def edit
  end

  def create
    @property_tenure = PropertyTenure.new(property_tenure_params)
    @property_tenure.save
    respond_with(@property_tenure)
  end

  def update
    @property_tenure.update(property_tenure_params)
    respond_with(@property_tenure)
  end

  def destroy
    @property_tenure.destroy
    respond_with(@property_tenure)
  end

  private
    def set_property_tenure
      @property_tenure = PropertyTenure.find(params[:id])
    end

    def property_tenure_params
      params.require(:property_tenure).permit(:name, :description)
    end
end

class FeedbacksController < ApplicationController
  before_action :authenticate_user!

  respond_to :html

  def index
    @feedbacks = Feedback.all
    respond_with(@feedbacks)
  end
  
  def destroy
    @feedback = Feedback.find(params[:id])
    @feedback.destroy
    respond_with(@feedback)
  end

end

class PropertyCategoriesController < ApplicationController
  before_action :set_property_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @property_categories = PropertyCategory.all
    respond_with(@property_categories)
  end

  def show
    respond_with(@property_category)
  end

  def new
    @property_category = PropertyCategory.new
    respond_with(@property_category)
  end

  def edit
  end

  def create
    @property_category = PropertyCategory.new(property_category_params)
    @property_category.save
    respond_with(@property_category)
  end

  def update
    @property_category.update(property_category_params)
    respond_with(@property_category)
  end

  def destroy
    @property_category.destroy
    respond_with(@property_category)
  end

  private
    def set_property_category
      @property_category = PropertyCategory.find(params[:id])
    end

    def property_category_params
      params.require(:property_category).permit(:name, :description)
    end
end

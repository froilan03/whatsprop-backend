class RoomLeasesController < ApplicationController
  before_action :set_room_lease, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @room_leases = RoomLease.all
    respond_with(@room_leases)
  end

  def show
    respond_with(@room_lease)
  end

  def new
    @room_lease = RoomLease.new
    respond_with(@room_lease)
  end

  def edit
  end

  def create
    @room_lease = RoomLease.new(room_lease_params)
    @room_lease.save
    respond_with(@room_lease)
  end

  def update
    @room_lease.update(room_lease_params)
    respond_with(@room_lease)
  end

  def destroy
    @room_lease.destroy
    respond_with(@room_lease)
  end

  private
    def set_room_lease
      @room_lease = RoomLease.find(params[:id])
    end

    def room_lease_params
      params.require(:room_lease).permit(:name, :description)
    end
end

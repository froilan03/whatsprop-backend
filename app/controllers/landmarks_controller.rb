class LandmarksController < ApplicationController
  before_action :set_landmark, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @landmarks = Landmark.all
    respond_with(@landmarks)
  end

  def show
    respond_with(@landmark)
  end

  def new
    @landmark = Landmark.new
    respond_with(@landmark)
  end

  def edit
  end

  def create
    @landmark = Landmark.new(landmark_params)
    @landmark.save
    respond_with(@landmark)
  end

  def update
    @landmark.update(landmark_params)
    respond_with(@landmark)
  end

  def destroy
    @landmark.destroy
    respond_with(@landmark)
  end

  private
    def set_landmark
      @landmark = Landmark.find(params[:id])
    end

    def landmark_params
      params.require(:landmark).permit(:name, :description)
    end
end

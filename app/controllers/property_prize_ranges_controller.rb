class PropertyPrizeRangesController < ApplicationController
  before_action :set_property_prize_range, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @property_prize_ranges = PropertyPrizeRange.all
    respond_with(@property_prize_ranges)
  end

  def show
    respond_with(@property_prize_range)
  end

  def new
    @property_prize_range = PropertyPrizeRange.new
    respond_with(@property_prize_range)
  end

  def edit
  end

  def create
    @property_prize_range = PropertyPrizeRange.new(property_prize_range_params)
    @property_prize_range.save
    respond_with(@property_prize_range)
  end

  def update
    @property_prize_range.update(property_prize_range_params)
    respond_with(@property_prize_range)
  end

  def destroy
    @property_prize_range.destroy
    respond_with(@property_prize_range)
  end

  private
    def set_property_prize_range
      @property_prize_range = PropertyPrizeRange.find(params[:id])
    end

    def property_prize_range_params
      params.require(:property_prize_range).permit(:name, :description)
    end
end

class ImageMultiUploaderController < ApplicationController
  def index
    @images = Image.all
    render :json => @images.collect { |p| p.to_jq_upload }.to_json
  end

  def new
    @image = Image.new # needed for form_for --> gets the path
  end

  def create
    @image = Image.new
    @image.attachment = params[:image][:path].shift
    if @image.save
      respond_to do |format|
        format.html { #(html response is for browsers using iframe sollution)
          render :json => [@image.to_jq_upload].to_json, :content_type => 'text/html', :layout => false
        }
        format.json {
          render :json => [@image.to_jq_upload].to_json
        }
      end
    else
      render :json => [{:error => "custom_failure"}], :status => 304
    end
  end

  def destroy
    @image = Image.find(params[:id])
    @image.destroy
    render :json => true
  end
end
class SettingsController < ApplicationController
  before_action :set_setting, only: [:edit, :update, :destroy]
  before_action :authenticate_user!
  
  respond_to :html

  def index
    @settings = Setting.all
    respond_with(@settings)
  end

  def new
    @setting = Setting.new
    respond_with(@setting)
  end

  def edit
  end

  def create
    @setting = Setting.new(setting_params)
    respond_to do |format|
      if @setting.save
        format.html { redirect_to settings_path }
      else
        format.html { render action: 'new' }
      end
    end
  end

  def update
    respond_to do |format|
      if @setting.update(setting_params)
        format.html { redirect_to settings_path }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  def destroy
    @setting.destroy
    respond_with(@setting)
  end

  private
    def set_setting
      @setting = Setting.find(params[:id])
    end

    def setting_params
      params.require(:setting).permit(:variable, :value)
    end
end

class HomeController < ApplicationController
  before_action :authenticate_user!
  
  def index
  end
  
  def dashboard
  end
  
  def export_posts
    if params[:type] == 'range'
      @posts = Post.where(created_at: 3.month.ago..Time.now)
      
      data = CSV.generate do |csv|
        @posts.each_with_index do |post, index|  
          if (index == 0)
            csv << ["Title", "Description", "Status", "Posted By", "Category", "Price", "Location", "Posted On"]  
          end
          if post.room.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Room for "+ post.room.room_category.name, post.room.price, post.room.location.address, post.created_at.strftime("%B %d, %Y")]
          elsif post.property.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Property for "+ post.property.property_category.name, post.property.price, post.property.location.address, post.created_at.strftime("%B %d, %Y")]
          end
        end 
      end
  
    elsif params[:type] == 'all'
      data = CSV.generate do |csv|
        csv << ["Title", "Description", "Status", "Posted By", "Category", "Price", "Location", "Posted On"] 
        
        @active = Post.active
        @active.each_with_index do |post, index|  
          if post.room.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Room for "+ post.room.room_category.name, post.room.price, post.room.location.address, post.created_at.strftime("%B %d, %Y")]
          elsif post.property.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Property for "+ post.property.property_category.name, post.property.price, post.property.location.address, post.created_at.strftime("%B %d, %Y")]
          end
        end
        
        csv << []        
        @inactive = Post.inactive
        @inactive.each_with_index do |post, index|  
          if post.room.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Room for "+ post.room.room_category.name, post.room.price, post.room.location.address, post.created_at.strftime("%B %d, %Y")]
          elsif post.property.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Property for "+ post.property.property_category.name, post.property.price, post.property.location.address, post.created_at.strftime("%B %d, %Y")]
          end
        end
        csv << []
        @draft = Post.draft
        @draft.each_with_index do |post, index|  
          if post.room.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Room for "+ post.room.room_category.name, post.room.price, post.room.location.address, post.created_at.strftime("%B %d, %Y")]
          elsif post.property.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Property for "+ post.property.property_category.name, post.property.price, post.property.location.address, post.created_at.strftime("%B %d, %Y")]
          end
        end
        
        csv << []
        @expired = Post.expired
        @expired.each_with_index do |post, index|  
          if post.room.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Room for "+ post.room.room_category.name, post.room.price, post.room.location.address, post.created_at.strftime("%B %d, %Y")]
          elsif post.property.present?
            csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, post.user.email, "Property for "+ post.property.property_category.name, post.property.price, post.property.location.address, post.created_at.strftime("%B %d, %Y")]
          end
        end
         
      end
      
    end
    
    respond_to do |format|
        format.csv { send_data data, filename: 'Posts.csv' }
    end
  end
  
  def export_post_views
    @post_views = PostView.all
    data = CSV.generate do |csv|
      csv << ["Post Title", "Post Description", "Post Status", "Category", "Viewed By", "Viewed On"]
      @post_views.each do |post_view|  
        post = post_view.post
        viewed_by = post_view.user.nil? ? "Guest" : post_view.user.email
        if post.room.present?
          csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, "Room for "+ post.room.room_category.name, viewed_by, post_view.created_at.strftime("%B %d, %Y")]
        elsif post.property.present?
          csv << [post.title.html_safe, post.description.html_safe, post.status.upcase!, "Property for "+ post.property.property_category.name, viewed_by, post_view.created_at.strftime("%B %d, %Y")]
        end
      end 
    end
      
    respond_to do |format|
        format.csv { send_data data, filename: 'PostViews.csv' }
    end
  end
  
  def export_users
    if params[:type] == 'all'
      @users = User.all
      data = CSV.generate do |csv|
        csv << ["Email", "Firstname", "Lastname", "Username", "Mobile", "Role", "Date Registered"]
        @users.each do |user|  
          csv << [user.email, user.first_name, user.last_name, user.username, user.mobile, user.user_role.name, user.created_at.strftime("%B %d, %Y")]
        end 
      end
    elsif params[:type] == 'range'
      @users = User.where(created_at: 1.month.ago..Time.now)
      data = CSV.generate do |csv|
        csv << ["Email", "Firstname", "Lastname", "Username", "Mobile", "Role", "Date Registered"]
        @users.each do |user|  
          csv << [user.email, user.first_name, user.last_name, user.username, user.mobile, user.user_role.name, user.created_at.strftime("%B %d, %Y")]
        end 
      end
    else
      if params[:type] == 'avg'
        @user_activity_logs = UserActivityLog.all  
        data = CSV.generate do |csv|
          csv << ["User", "Dwell Time (ms)"]
          @user_activity_logs.each do |activity|  
            csv << [activity.user.email, activity.dwell_time + " ms"]
          end 
        end
      
      elsif params[:type] == 'unique'
        @guest_activity_logs = GuestActivityLog.all  
        data = CSV.generate do |csv|
          csv << ["Action", "Created At"]
          @guest_activity_logs.each do |activity|  
            csv << [activity.action, activity.created_at.strftime("%B %d, %Y %H:%M:%S %p")]
          end 
        end
      end   
    end
    
    respond_to do |format|
        format.csv { send_data data, filename: 'Users.csv' }
    end
  end
  
  def export_shares
    if params[:share_type] == 'posts'
      fname = 'PostsShared.csv'
      @post_shares = PostShare.all
      data = CSV.generate do |csv|
        csv << ["Post Title", "Shared By", "Email", "Share Type", "Created At"]
        @post_shares.each do |share|  
          csv << [share.post.title, share.user.email, share.email, share.share_type, share.created_at.strftime("%B %d, %Y %H:%M:%S %p")]
        end 
      end
      
    elsif params[:share_type] == 'apps'
      fname = 'AppsShared.csv'
      @app_shares = AppShare.all  
      data = CSV.generate do |csv|
        csv << ["Shared By", "Email", "Share Type", "Created At"]
        @app_shares.each do |share|  
          csv << [share.user.email, share.email, share.share_type, share.created_at.strftime("%B %d, %Y %H:%M:%S %p")]
        end 
      end
    end
    
    respond_to do |format|
        format.csv { send_data data, filename: fname }
    end
  end
  
  def export_feedbacks
    @feedbacks = Feedback.all
    data = CSV.generate do |csv|
        csv << ["Feedback From", "Message", "Created At"]
        @feedbacks.each do |fb|  
          csv << [fb.user.email, fb.message, fb.created_at.strftime("%B %d, %Y %H:%M:%S %p")]
        end 
      end
      
    respond_to do |format|
        format.csv { send_data data, filename: "Feedbacks.csv" }
    end
  end
end

class ImageUploaderService
  def initialize(params)
    @params = params
  end

  def upload
    #@params[:image] = parse_image_data(@params[:image]) if @params[:image]
    #@model.update(@params)
    Rails.logger.info ">> image: #{@params[:image]}"
    parse_image_data(@params[:image]) if @params[:image]
  ensure
    clean_tempfile
  end

  private
  def parse_image_data(image_data)
    @tempfile = Tempfile.new('image-data-temp')
    @tempfile.binmode
    @tempfile.write Base64.decode64(image_data[:file_data])
    @tempfile.rewind

    ActionDispatch::Http::UploadedFile.new(
      :tempfile => @tempfile,
      :content_type => image_data[:content_type],
      :filename => image_data[:filename]
    )
    
  end

  def clean_tempfile
    if @tempfile
      @tempfile.close
      @tempfile.unlink
    end
  end

end
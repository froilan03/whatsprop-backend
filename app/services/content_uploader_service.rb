class ContentUploaderService
  def initialize(content_data, content_file_name, content_type)
    @content_data = content_data
    @content_type = content_type
    #@asset_type = asset_type
    @content_file_name = content_file_name
  end

  def upload
    upload_file if @content_data
  ensure
    clean_tempfile
  end

  private
  def upload_file
    data = StringIO.new(Base64.decode64(@content_data))
        data.class_eval do
          attr_accessor :content_type, :original_filename
        end
 
        data.content_type = @content_type
        data.original_filename = File.basename(@content_file_name)
    data    
  end
  
  def process_data
    @tempfile = Tempfile.new('image-data-temp')
    @tempfile.binmode
    @tempfile.write Base64.decode64(@content_data)
    @tempfile.rewind
    
    uploaded_file = ActionDispatch::Http::UploadedFile.new(
      :tempfile => @tempfile,
      :filename => @content_file_name
    )
    
    uploaded_file.content_type = @content_type
    uploaded_file
  end

  def clean_tempfile
    if @tempfile
      @tempfile.close
      @tempfile.unlink
    end
  end

end
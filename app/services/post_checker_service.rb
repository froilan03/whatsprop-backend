class PostCheckerService
  def self.check_post_expiration
    @posts = Post.active
    @posts.each do |post|
      unless post.expiry_date.nil?
        days_left = (post.expiry_date - Date.today).to_i  
      
        if days_left == 2
          UserMailer.send_post_expiry_notification(post).deliver  
        end
      end
    end
  end
end
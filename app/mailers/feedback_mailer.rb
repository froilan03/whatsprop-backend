class FeedbackMailer < ActionMailer::Base
  default from: "\"WhatsProp\" <mailer@thepineapps.com>"
  
  def send_feedback(uid, msg)
    @user = User.find_by_id uid
    @message = msg
    mail(to: @user.email, subject: 'WhatsProp - User Feedback Notification', reply_to: "marketing@thepineapps.com")  
  end
  
  def forward_to_marketing(msg)
    @message = msg
    mail(to: 'marketing@thepineapps.com', subject: 'WhatsProp - User Feedback Notification')  
  end
end
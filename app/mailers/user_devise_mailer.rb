class UserDeviseMailer < Devise::Mailer
  helper :application
  include Devise::Mailers::Helpers

  default from: Proc.new { email_from },
            subject: Proc.new { email_subject }
            
  def confirmation_instructions(record, token, opts={})
    opts[:subject] = "Welcome to WhatsProp, Please Verify Your New Account"
    super
  end
  
  def reset_password_instructions(record, token, opts={})
    @user = record

    today = Time.now
    tomorrow = today + (60 * 60 * 24)
    @expires_at = tomorrow.strftime('%B %d, %Y %I:%M:%S %p')

    super
  end

  # override this to avoid sending unlock instruction
  def unlock_instructions(record, token, opts={})
    @user = record
    super
  end

  def welcome_user
    
  end
  
  private

  def email_subject
    "Test"#@email_subj 
  end

  def email_from
    "mailer@thepineapps.com"
  end
end
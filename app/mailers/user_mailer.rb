class UserMailer < ActionMailer::Base
  default from: "\"WhatsProp\" <mailer@thepineapps.com>"
  
  def welcome_email(user)
    @user = user
    
    mail(to: @user.email, subject: 'Welcome to WhatsProp')
  end
  
  def share_post_email(email, post_id)
    @post = Post.find post_id 
    mail(to: email, subject: 'WhatsProp - Post Details')  
  end
  
  def share_app_email(email)
    mail(to: email, subject: 'WhatsProp - Share App')  
  end
  
  def send_post_expiry_notification post
    @post = post
    mail(to: post.user.email, subject: 'WhatsProp - Post Expiry Notification')
  end
  
  def email_chat_message(sender, recipient, message)
    @sender = User.find_by_id sender
    @recipient = User.find_by_id recipient
    @message = message
    @name = @sender.first_name.to_s + " " + @sender.last_name.to_s
    @subj =  @name + " sent you 1 message in WhatsProp"
    
    mail(to: @recipient.email, subject: @subj)
  end
  
  def send_credit_notification post
    @post = post
    mail(to: post.user.email, subject: 'WhatsProp - Credit Balance Notification')
  end
end
class UserMailer < ActionMailer::Base
  before_action :set_email_settings
  after_action :set_smtp
  
  default from: Proc.new { default_from },
          subject: Proc.new { email_subject }
  
  def itinerary_email(recipient, attached_filename)
    begin
      email = mail(:to => recipient)
      message_content = @email_content
     
      # pdf attachment
      pdf_path = File.join(Rails.root.to_s, "public", "email", "#{attached_filename}.pdf")
      if File.exists?(pdf_path)
        attachments["#{attached_filename}.pdf"] = File.read(pdf_path)
      end
      
      html_container = Mail::Part.new { content_type 'multipart/related' }
      html_container.add_part(
        Mail::Part.new do
        content_type 'text/html; charset=UTF-8'
          body message_content
        end
      )
  
      alternative_bodies = Mail::Part.new { content_type 'multipart/alternative' }
      alternative_bodies.add_part(html_container)
      
      email.add_part(alternative_bodies)
    rescue Exception => e
      logger.error ">>>>>>>>>>>>> Error occured while sending itinerary email. #{e}"
      raise e
    end  
  end
  
  private
    def email_subject
      SsApplicationSetting.first.nil? ? 'Itinerary Details' : SsApplicationSetting.first.itinerary_email_subject
    end
      
    def default_from
      EmailPreference.first.username
    end
      
    def set_smtp
      email = EmailPreference.first
      if email
        settings = {
          :address =>              email.address,
          :port =>                 email.port,
          :domain =>               email.domain,
          :user_name =>            email.username,
          :password =>             email.password,
          :authentication =>       email.authenticate_type,
          :enable_starttls_auto => true  
        }
        
        mail.delivery_method.settings.merge!(settings)
        logger.info "Settings: #{mail.delivery_method.settings}"
      end
    end
    
    def set_email_settings
      ss_settings = SsApplicationSetting.first
      unless ss_settings.nil?
        @email_content = ss_settings.itinerary_email_body
      end
    end
end

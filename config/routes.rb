Rails.application.routes.draw do
  root "home#index"
  
  devise_for :users, :path_prefix => 'auth', :skip => [:registrations],
    :controllers => {
        :sessions => "sessions",
    }
    
  # api routes
  namespace :api, defaults: {format: :json} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      
      # devise api routes
      # sign in, sign out, sign up
      devise_for :users, skip: :all
      devise_scope :api_user do
        post '/users/sign_in' => 'sessions#create'
        post '/users/sign_out' => 'sessions#destroy'
        post '/users/sign_up' => 'registrations#create'
      end
      
      # users api routes
      # update user, upload avatar, remove avatar
      get '/users/available_roles' => 'users#available_roles'
      post '/users/update/:id' => 'users#update'
      post '/users/upload_avatar/:id' => 'users#upload_avatar'
      post '/users/change_password/:id' => 'users#change_password'
      post '/users/remove_avatar/:id' => 'users#remove_avatar'
      get '/users/user_info' => 'users#user_info'
            
      # posts api routes
      get '/posts' => 'posts#index'
      get '/posts/current_user_posts' => 'posts#current_user_posts'
      get '/posts/info' => 'posts#info'
      get '/posts/get_by_location' => 'posts#get_by_location'
      get '/posts/map_posts_listings' => 'posts#map_posts_listings'
      post '/posts/create' => 'posts#create'
      post '/posts/update/:id' => 'posts#update'
      post '/posts/destroy' => 'posts#destroy'
      post '/posts/publish' => 'posts#publish'
      post '/posts/unpublish' => 'posts#unpublish'
      post '/posts/upload_asset' => 'posts#upload_asset'
      post '/posts/destroy_asset' => 'posts#destroy_asset'
      get '/posts/get_posts_count' => 'posts#get_post_count_by_location_with_radius'
      get '/posts/get_post_messages' => 'posts#get_post_messages'
      get '/posts/get_post_message_info' => 'posts#get_post_message_info'
      get '/posts/get_message' => 'posts#get_message'
      
      # servicing agents api routes
      get '/servicing_agents' => 'servicing_agents#index'
      get '/servicing_agents/info' => 'servicing_agents#info'
      get '/servicing_agents/my_services/:id' => 'servicing_agents#my_services'
      post '/servicing_agents/create' => 'servicing_agents#create'
      post '/servicing_agents/update/:id' => 'servicing_agents#update'
      post '/servicing_agents/destroy/:id' => 'servicing_agents#destroy'
      
      # services api routes
      get '/services' => 'services#index'
      get '/services/info/:id' => 'services#info'
      get '/services/my_services/:id' => 'services#my_services'
      post '/services/create' => 'services#create'
      post '/services/update/:id' => 'services#update'
      post '/services/destroy/:id' => 'services#destroy'
      
      # advance search api routes
      get '/search/posts' => 'search#index'
      
      # bookmrk api routes
      get '/bookmarks' => 'bookmarks#index'
      get '/bookmarks/:user_id' => 'bookmarks#index'
      post '/bookmarks/create' => 'bookmarks#create'
      post '/bookmarks/delete' => 'bookmarks#destroy'
     
      # miscellaneous api routes
      get 'misc/:type' => 'miscellaneous#index'
      
      # share api routes
      post 'share/app' => 'share#share_app'
      post 'share/post' => 'share#share_post'
      
      # feedback api routes
      post 'feedback/send' => 'feedback#leave_feedback'
      
      # announcements api routes
      get '/announcements' => 'announcements#index'
      get '/announcements/info/:id' => 'announcements#info'
      
      get 'chat/hello_world' => "chat#hello_world"
      post 'chat/webhooks' => "chat#webhooks"
      post 'chat/auth' => "chat#auth"
      post 'chat/email_message' => "chat#email_message"
      get 'chat/my_messages' => "chat#my_messages"
      get 'chat/message_info/:id' => "chat#message_info"
      post 'chat/delete_message' => "chat#delete_message"
      
      #analytics
      post 'analytics/user/activity' => "analytics#user_activity_log"
      post 'analytics/guest/activity' => "analytics#guest_activity_log"
      
      
      post 'messages/:id/:status' => "messages#updatestatus"
      get 'messages/:user_id/unread' => "messages#get_unread"
      delete 'messages/:id/:type' => "messages#delete_message"
      
      # regions & city api routes
      get '/regions' => 'regions#index'
      
      resources :conversations do
        resources :messages
      end 

    end
  end
  
   # admin routes 
  scope :admin do
    get 'home' => 'home#index'
    get 'home/export_posts/:type/posts' => 'home#export_posts'
    get 'home/export_users/:type/users' => 'home#export_users'
    get 'home/export_post_views' => 'home#export_post_views'
    get 'home/export_shares/:share_type/shared' => 'home#export_shares'
    get 'home/export_feedbacks' => 'home#export_feedbacks'
    
    resources :users, only: [:index, :new, :edit, :show, :create, :update, :destroy] do
      post 'block'
      post 'unblock'
      get 'edit_profile'
    end
    
    resources :posts
    resources :admin_posts   
    resources :properties
    resources :property_tenures
    resources :property_types
    resources :property_sizes
    resources :property_psfs
    resources :property_price_ranges
    resources :property_categories
    resources :property_year_constructeds
    
    resources :rooms
    resources :room_share_genders
    resources :lease_terms
    resources :furnishings
    resources :room_categories
    resources :room_bathroom_types

    resources :landmarks
  
    resources :servicing_agents
    
    resources :share_apps, only: [:index, :destroy]
    
    resources :feedbacks, only: [:index, :destroy]
    
    resources :images
    
    resources :announcements
    
    resources :settings
    
    resources :refer_codes
    
    resources :regions
    
    resources :cities
  end
  
  scope :web_account do
    get '/' => 'web_accounts#index'
    get 'credits' => 'web_accounts#index'
    post 'credits' => 'web_accounts#index'
    get 'edit_profile' => 'web_accounts#edit_profile'
    get 'change_password' => 'web_accounts#change_password'
    patch 'update' => 'web_accounts#update'
    patch 'update_password' => 'web_accounts#update_password'
    
    post "credits/hook" => "web_accounts#hook"
    get 'credits/buy' => 'account_credits#index'
    post 'credits/payment' => 'account_credits#payment'
  end
  
  get 'account/terms_conditions' => 'terms_conditions#index'
  get 'post/details/:id' => 'posts#details'
  
# The priority is based upon order of creation: first created -> highest priority.
# See how all your routes lay out with "rake routes".

# You can have the root of your site routed with "root"
# root 'welcome#index'

# Example of regular route:
#   get 'products/:id' => 'catalog#view'

# Example of named route that can be invoked with purchase_url(id: product.id)
#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

# Example resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Example resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Example resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Example resource route with more complex sub-resources:
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', on: :collection
#     end
#   end

# Example resource route with concerns:
#   concern :toggleable do
#     post 'toggle'
#   end
#   resources :posts, concerns: :toggleable
#   resources :photos, concerns: :toggleable

# Example resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end
end

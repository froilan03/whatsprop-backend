class CreateServicingAgents < ActiveRecord::Migration
  def change
    create_table :servicing_agents do |t|
      t.string :title
      t.string :description
      t.float :price
      t.integer :user_id

      t.timestamps
    end
  end
end

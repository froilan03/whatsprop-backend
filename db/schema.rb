# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150213170518) do

  create_table "account_credits", force: true do |t|
    t.integer  "user_id",                                       null: false
    t.float    "amount",              limit: 24,  default: 0.0, null: false
    t.string   "transaction_id",      limit: 150
    t.string   "status",              limit: 150
    t.text     "notification_params"
    t.datetime "purchased_at"
    t.datetime "updated_at"
  end

  add_index "account_credits", ["amount"], name: "user_two", using: :btree
  add_index "account_credits", ["user_id"], name: "user_one", using: :btree

  create_table "agent_info", force: true do |t|
    t.integer   "user_id",                                 null: false
    t.string    "reg_no",                      limit: 20,  null: false
    t.date      "reg_period_start",                        null: false
    t.date      "reg_period_end",                          null: false
    t.string    "estate_agent_name",           limit: 100, null: false
    t.string    "estate_agent_license_number", limit: 100, null: false
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "announcements", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "app_shares", force: true do |t|
    t.string   "email",      limit: 128
    t.integer  "user_id"
    t.string   "share_type", limit: 20
    t.datetime "created_at"
  end

  create_table "bookmarks", force: true do |t|
    t.integer   "post_id",    null: false
    t.integer   "user_id",    null: false
    t.timestamp "created_at", null: false
    t.timestamp "updated_at"
  end

  add_index "bookmarks", ["post_id"], name: "FK_post_user_id", using: :btree

  create_table "content_type", force: true do |t|
    t.string "name",        limit: 10
    t.string "description", limit: 50
  end

  create_table "conversation", primary_key: "c_id", force: true do |t|
    t.integer   "user_one",              null: false
    t.integer   "user_two",              null: false
    t.string    "ip",         limit: 30
    t.timestamp "time",                  null: false
    t.integer   "post_id_fk",            null: false
  end

  add_index "conversation", ["user_one"], name: "user_one", using: :btree
  add_index "conversation", ["user_two"], name: "user_two", using: :btree

  create_table "conversation_reply", primary_key: "cr_id", force: true do |t|
    t.text      "reply"
    t.integer   "user_id_fk",            null: false
    t.string    "ip",         limit: 30, null: false
    t.timestamp "time",                  null: false
    t.integer   "c_id_fk",               null: false
  end

  add_index "conversation_reply", ["c_id_fk"], name: "c_id_fk", using: :btree
  add_index "conversation_reply", ["user_id_fk"], name: "user_id_fk", using: :btree

  create_table "conversations", force: true do |t|
    t.integer  "sender_id",    null: false
    t.integer  "recipient_id", null: false
    t.datetime "created_at"
  end

  add_index "conversations", ["recipient_id"], name: "recipient", using: :btree
  add_index "conversations", ["sender_id"], name: "sender", using: :btree

  create_table "feedbacks", force: true do |t|
    t.string   "message"
    t.integer  "user_id"
    t.datetime "created_at"
  end

  create_table "furnishings", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "guest_activity_logs", force: true do |t|
    t.string   "action"
    t.datetime "created_at"
  end

  create_table "landmarks", force: true do |t|
    t.string   "name",        limit: 11, null: false
    t.string   "description", limit: 11, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lease_terms", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.string  "address",   limit: 200
    t.decimal "latitude",              precision: 10, scale: 6
    t.decimal "longitude",             precision: 10, scale: 6
  end

  create_table "messages", force: true do |t|
    t.text     "body"
    t.integer  "user_id",         null: false
    t.integer  "conversation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["conversation_id"], name: "conversation", using: :btree
  add_index "messages", ["user_id"], name: "user", using: :btree

  create_table "post_content_gallery", force: true do |t|
    t.string   "type",                    limit: 128
    t.integer  "assetable_id",                        null: false
    t.string   "assetable_type",          limit: 128
    t.string   "attachment_file_name",    limit: 256
    t.string   "attachment_content_type", limit: 128
    t.integer  "attachment_file_size"
    t.integer  "content_type_id"
    t.datetime "attachment_updated_at"
  end

  create_table "post_landmarks", force: true do |t|
    t.string  "description", limit: 200, null: false
    t.integer "landmark_id",             null: false
    t.integer "post_id",                 null: false
  end

  create_table "post_shares", force: true do |t|
    t.integer  "post_id"
    t.integer  "user_id"
    t.string   "email",      limit: 128
    t.string   "share_type", limit: 128
    t.datetime "created_at"
  end

  create_table "post_views", force: true do |t|
    t.integer  "post_id",    null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at"
  end

  add_index "post_views", ["user_id"], name: "FK_post_user_id", using: :btree

  create_table "posts", force: true do |t|
    t.string   "title",       limit: 300,                    null: false
    t.string   "description", limit: 2000,                   null: false
    t.string   "status",      limit: 20,   default: "draft"
    t.integer  "user_id",                                    null: false
    t.date     "expiry_date"
    t.string   "posted_by",   limit: 50,   default: "user"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "posts", ["user_id"], name: "FK_post_user_id", using: :btree

  create_table "property", force: true do |t|
    t.string    "name",                 limit: 200
    t.integer   "number_of_bed",                     null: false
    t.integer   "number_of_bath",                    null: false
    t.integer   "year_constructed"
    t.float     "price",                limit: 24
    t.float     "size",                 limit: 24,   null: false
    t.string    "developer",            limit: 200
    t.string    "features",             limit: 3000
    t.string    "amenities",            limit: 3000
    t.float     "psf",                  limit: 24
    t.integer   "post_id"
    t.integer   "location_id"
    t.integer   "property_type_id",                  null: false
    t.integer   "property_tenure_id"
    t.integer   "property_category_id",              null: false
    t.integer   "lease_term_id"
    t.integer   "furnishing_id"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  add_index "property", ["property_category_id"], name: "FK_property_category", using: :btree

  create_table "property_category", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "property_price_range", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.integer  "range_to"
    t.integer  "range_from"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "property_psf", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "property_size", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.integer  "range_from"
    t.integer  "range_to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "property_tenure", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "property_types", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "property_year_constructed", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "refer_codes", force: true do |t|
    t.string   "name"
    t.string   "code_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "room_bathroom_types", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "room_category", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "room_share_gender", force: true do |t|
    t.string   "name",        limit: 200
    t.string   "description", limit: 200
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rooms", force: true do |t|
    t.float    "price",                 limit: 24
    t.string   "cooking",               limit: 1
    t.float    "size",                  limit: 24
    t.string   "share_living_room",     limit: 1
    t.integer  "post_id"
    t.integer  "location_id"
    t.integer  "property_type_id",                 null: false
    t.integer  "furnishing_id",                    null: false
    t.integer  "room_bathroom_type_id",            null: false
    t.integer  "room_category_id",                 null: false
    t.integer  "lease_term_id",                    null: false
    t.integer  "room_share_gender_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at"
  end

  create_table "servicing_agents", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.float    "price",       limit: 24
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "settings", force: true do |t|
    t.string   "variable"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_activity_logs", force: true do |t|
    t.string   "action"
    t.integer  "user_id"
    t.datetime "created_at"
  end

  create_table "user_roles", force: true do |t|
    t.string "name",        limit: 30,  null: false
    t.string "description", limit: 100
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile"
    t.integer  "user_role_id"
    t.string   "avatar_file_name"
    t.string   "authentication_token"
    t.string   "avatar_content_type"
    t.string   "email",                              null: false
    t.string   "refer_code"
    t.boolean  "access_locked"
    t.string   "encrypted_password",                 null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
